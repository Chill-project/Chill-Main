<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Tests\PasswordRecover;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\MainBundle\Security\PasswordRecover\TokenManager;
use Chill\MainBundle\Entity\User;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TokenManagerTest extends KernelTestCase
{
    protected $tokenManager;
    
    public static function setUpBefore()
    {
        
    }
    
    public function setUp()
    {
        self::bootKernel();
        
        $logger = self::$kernel
            ->getContainer()
            ->get('logger');
        
        $this->tokenManager = new TokenManager('secret', $logger);
    }

    public function testGenerate() 
    {
        $tokenManager = $this->tokenManager;
        $user = (new User())->setUsernameCanonical('test');
        $expiration = new \DateTimeImmutable('tomorrow');
        
        $tokens = $tokenManager->generate($user, $expiration);
        
        $this->assertInternalType('array', $tokens);
        $this->assertArrayHasKey('h', $tokens);
        $this->assertArrayHasKey('t', $tokens);
        $this->assertNotEmpty($tokens['h']);
        $this->assertNotEmpty($tokens['t']);
        $this->assertEquals($user->getUsernameCanonical(), $tokens['u']);
    }
    
    /**
     * @expectedException \UnexpectedValueException
     */
    public function testGenerateEmptyUsernameCanonical() 
    {
        $tokenManager = $this->tokenManager;
        // set a username, but not a username canonical
        $user = (new User())->setUsername('test');
        $expiration = new \DateTimeImmutable('tomorrow');
        
        $tokenManager->generate($user, $expiration);
    }
    
    public function testVerify()
    {
        $tokenManager = $this->tokenManager;
        $user = (new User())->setUsernameCanonical('test');
        $expiration = new \DateTimeImmutable('tomorrow');
        
        $tokens = $tokenManager->generate($user, $expiration);
        
        $hash = $tokens[TokenManager::HASH];
        $token = $tokens[TokenManager::TOKEN];
        $timestamp = $tokens[TokenManager::TIMESTAMP];
        
        $verification = $tokenManager->verify($hash, $token, $user, $timestamp);
        
        $this->assertTrue($verification);
        
        // test with altering token
        $this->assertFalse($tokenManager->verify($hash.'5', $token, $user, $timestamp));
        $this->assertFalse($tokenManager->verify($hash, $token.'25', $user, $timestamp));
        $this->assertFalse($tokenManager->verify($hash, $token, $user->setUsernameCanonical('test2'), $timestamp));
        $this->assertFalse($tokenManager->verify($hash, $token, $user, $timestamp+1));
    }
    
    public function testVerifyExpiredFails()
    {
        $tokenManager = $this->tokenManager;
        $user = (new User())->setUsernameCanonical('test');
        $expiration = (new \DateTimeImmutable('now'))->sub(new \DateInterval('PT1S'));
        
        $tokens = $tokenManager->generate($user, $expiration);
        
        $hash = $tokens[TokenManager::HASH];
        $token = $tokens[TokenManager::TOKEN];
        $timestamp = $tokens[TokenManager::TIMESTAMP];
        
        $verification = $tokenManager->verify($hash, $token, $user, $timestamp);
        
        $this->assertFalse($verification);
    }
}
