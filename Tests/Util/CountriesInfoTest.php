<?php

namespace Chill\MainBundle\Tests\Util;

use PHPUnit\Framework\TestCase;
use Chill\MainBundle\Util\CountriesInfo;

/**
 * tests for CountriesInfos
 *
 * @author julien.fastre@champs-libre.coop
 */
class CountriesInfoTest extends TestCase {
    
    public function testGetCountryData() 
    {
        $raw = CountriesInfo::getCountriesData();
        
        $this->assertStringStartsWith("AS AF AFG 004 Afghanistan, Islamic Republic of",
                $raw);
    }
    
    public function testGetArrayCountriesData() 
    {
        $data = CountriesInfo::getArrayCountriesData();
        
        $this->assertNotNull($data);
        $this->assertContains(array(
            "AS", "AF", "AFG", "004", "Afghanistan, Islamic Republic of"
        ), $data);
    }
    
    public function testGetCountryCodeByContinents()
    {
        $countries = CountriesInfo::getCountriesCodeByContinent('EU');
        
        $this->assertContains('BE', $countries);
        $this->assertContains('FR', $countries);
        $this->assertContains('GB', $countries);
    }
    
    public function getGetContinentsCodes()
    {
        $continents = CountriesInfo::getContinentsCodes();
        
        $this->assertContains('EU', $continents);
    }
}
