<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Entity\PostalCode;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Chill\MainBundle\Form\ChoiceLoader\PostalCodeChoiceLoader;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * A form to pick between PostalCode
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class PostalCodeType extends AbstractType
{
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
     *
     * @var PostalCodeChoiceLoader
     */
    protected $choiceLoader;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct(
        TranslatableStringHelper $helper,
        UrlGeneratorInterface $urlGenerator,
        PostalCodeChoiceLoader $choiceLoader,
        TranslatorInterface $translator
    ) {
        $this->translatableStringHelper = $helper;
        $this->urlGenerator = $urlGenerator;
        $this->choiceLoader = $choiceLoader;
        $this->translator = $translator;
    }


    public function getParent()
    {
        return EntityType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // create a local copy for usage in Closure
        $helper = $this->translatableStringHelper;
        $resolver
            ->setDefault('class', PostalCode::class)
            ->setDefault('choice_label', function(PostalCode $code) use ($helper) {
                return $code->getCode().' '.$code->getName().' ['.
                      $helper->localize($code->getCountry()->getName()).']';
           })
           ->setDefault('choice_loader', $this->choiceLoader)
           ->setDefault('placeholder', 'Select a postal code')
        ;
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['attr']['data-postal-code'] = 'data-postal-code';
        $view->vars['attr']['data-select-interactive-loading'] = true;
        $view->vars['attr']['data-search-url'] = $this->urlGenerator
            ->generate('chill_main_postal_code_search');
        $view->vars['attr']['data-placeholder'] = $this->translator->trans($options['placeholder']);
        $view->vars['attr']['data-no-results-label'] = $this->translator->trans('select2.no_results');
        $view->vars['attr']['data-error-load-label'] = $this->translator->trans('select2.error_loading');
        $view->vars['attr']['data-searching-label'] = $this->translator->trans('select2.searching');
    }

}
