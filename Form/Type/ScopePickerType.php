<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\MainBundle\Entity\Scope;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Entity\Center;
use Symfony\Component\Security\Core\Role\Role;

/**
 * Allow to pick amongst available scope for the current
 * user.
 *
 * options :
 *
 * - `center`: the center of the entity
 * - `role`  : the role of the user
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ScopePickerType extends AbstractType
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     *
     * @var EntityRepository
     */
    protected $scopeRepository;

    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(
        AuthorizationHelper $authorizationHelper,
        TokenStorageInterface $tokenStorage,
        EntityRepository $scopeRepository,
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->authorizationHelper = $authorizationHelper;
        $this->tokenStorage = $tokenStorage;
        $this->scopeRepository = $scopeRepository;
        $this->translatableStringHelper = $translatableStringHelper;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            // create `center` option
            ->setRequired('center')
            ->setAllowedTypes('center', [Center::class ])
            // create ``role` option
            ->setRequired('role')
            ->setAllowedTypes('role', ['string', Role::class ])
            ;

        $resolver
            ->setDefault('class', Scope::class)
            ->setDefault('placeholder', 'Choose the circle')
            ->setDefault('choice_label', function(Scope $c) {
                return $this->translatableStringHelper->localize($c->getName());
            })
            ->setNormalizer('query_builder', function(Options $options) {
                return $this->buildAccessibleScopeQuery($options['center'], $options['role']);
            })
            ;
    }

    public function getParent()
    {
        return EntityType::class;
    }
    
    /**
     * 
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function buildAccessibleScopeQuery(Center $center, Role $role)
    {
        $roles = $this->authorizationHelper->getParentRoles($role);
        $roles[] = $role;
        
        $qb = $this->scopeRepository->createQueryBuilder('s');
        $qb
            // jointure to center
            ->join('s.roleScopes', 'rs')
            ->join('rs.permissionsGroups', 'pg')
            ->join('pg.groupCenters', 'gc')
            // add center constraint
            ->where($qb->expr()->eq('IDENTITY(gc.center)', ':center'))
            ->setParameter('center', $center->getId())
            // role constraints
            ->andWhere($qb->expr()->in('rs.role', ':roles'))
            ->setParameter('roles', \array_map(
                    function(Role $role) {
                        return $role->getRole();
                    },
                    $roles
                ))
            // user contraint
            ->andWhere(':user MEMBER OF gc.users')
            ->setParameter('user', $this->tokenStorage->getToken()->getUser())
            ;
        
        return $qb;
    }
}
