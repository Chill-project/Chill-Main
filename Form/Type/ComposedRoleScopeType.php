<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2015 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Security\RoleProvider;


/**
 * Form to Edit/create a role scope. If the role scope does not
 * exists in the database, he is generated.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class ComposedRoleScopeType extends AbstractType
{
    /**
     *
     * @var string[]
     */
    private $roles = array();

    /**
     *
     * @var string[]
     */
    private $rolesWithoutScope = array();

    /**
     *
     * @var TranslatableStringHelper
     */
    private $translatableStringHelper;

    /**
     *
     * @var RoleProvider
     */
    private $roleProvider;

    public function __construct(
        TranslatableStringHelper $translatableStringHelper,
        RoleProvider $roleProvider
    ) {
        $this->roles = $roleProvider->getRoles();
        $this->rolesWithoutScope = $roleProvider->getRolesWithoutScopes();
        $this->translatableStringHelper = $translatableStringHelper;
        $this->roleProvider = $roleProvider;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // store values used in internal function
        $translatableStringHelper = $this->translatableStringHelper;
        $rolesWithoutScopes = $this->rolesWithoutScope;

        //build roles
        $values = array();
        foreach ($this->roles as $role) {
            $values[$role] = $role;
        }

        $builder
            ->add('role', ChoiceType::class, array(
               'choices' => array_combine(array_values($values),array_keys($values)),
               'choices_as_values' => true,
               'placeholder' => 'Choose amongst roles',
               'choice_attr' => function($role) use ($rolesWithoutScopes) {
                    if (in_array($role, $rolesWithoutScopes)) {
                        return array('data-has-scope' => '0');
                    } else {
                        return array('data-has-scope' => '1');
                    }
               },
               'group_by' => function($role, $key, $index) {
                    return $this->roleProvider->getRoleTitle($role);
               }
            ))
            ->add('scope', EntityType::class, array(
                'class' => 'ChillMainBundle:Scope',
                'choice_label' => function(Scope $scope) use ($translatableStringHelper) {
                    return $translatableStringHelper->localize($scope->getName());
                },
                'required' => false,
                'data' => null
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', 'Chill\MainBundle\Entity\RoleScope');
    }

}
