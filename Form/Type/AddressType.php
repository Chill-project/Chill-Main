<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Chill\MainBundle\Entity\Address;
use Chill\MainBundle\Form\Type\PostalCodeType;
use Chill\MainBundle\Form\DataMapper\AddressDataMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * A type to create/update Address entity
 *
 * Options:
 * 
 * - `has_valid_from` (boolean): show if an entry "has valid from" must be 
 * shown.
 * - `null_if_empty` (boolean): replace the address type by null if the street
 * or the postCode is empty. This is useful when the address is not required and
 * embedded in another form.
 * - `customize_data` (callable): allow to add custom field to the form, which
 * will be store in the `customs` property
 */
class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
              ->add('streetAddress1', TextType::class, array(
                 'required' => !$options['has_no_address'] // true if has no address is false
              ))
              ->add('streetAddress2', TextType::class, array(
                 'required' => false
              ))
              ->add('postCode', PostalCodeType::class, array(
                 'label' => 'Postal code',
                 'placeholder' => 'Choose a postal code',
                 'required' => !$options['has_no_address'] // true if has no address is false
              ))
            ;
        
        if ($options['has_valid_from']) {
            $builder
              ->add('validFrom', DateType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
                 )
              );
        }
        
        if ($options['has_no_address']) {
            $builder
                ->add('isNoAddress', ChoiceType::class, [
                    'required' => true,
                    'choices' => [
                        'address.consider homeless' => true,
                        'address.real address' => false
                    ],
                    'label' => 'address.address_homeless'
                ]);
        }
        
        if ($options['customize_data'] !== NULL && is_callable($options['customize_data'])) {
            $customsBuilder = $builder->create('customs', NULL, [ 
                'compound' => true,
                'label' => false
                ]);
            \call_user_func($options['customize_data'], $customsBuilder);
            $builder->add($customsBuilder);
        }
        
        if ($options['null_if_empty'] === TRUE) {
            $builder->setDataMapper(new AddressDataMapper());
        }

    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', Address::class)
            ->setDefined('has_valid_from')
            ->setAllowedTypes('has_valid_from', 'bool')
            ->setDefault('has_valid_from', true)
            ->setDefined('has_no_address')
            ->setDefault('has_no_address', false)
            ->setAllowedTypes('has_no_address', 'bool')
            ->setDefined('null_if_empty')
            ->setDefault('null_if_empty', false)
            ->setAllowedTypes('null_if_empty', 'bool')
            ->setDefined('customize_data')
            ->setAllowedTypes('customize_data', 'callable')
            ;
    }
}
