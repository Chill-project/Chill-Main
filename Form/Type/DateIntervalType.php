<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\MainBundle\Form\Type\DataTransformer\DateIntervalTransformer;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;


/**
 * Show a dateInterval type
 * 
 * Options:
 * 
 * - `unit_choices`: an array of available units choices. 
 * 
 * The oiriginal `unit_choices` are : 
 * ```
 * [
 *      'Days' => 'D',
 *      'Weeks' => 'W',
 *      'Months' => 'M',
 *      'Years' => 'Y'
 * ]
 * ```
 * 
 * You can remove one or more entries:
 * 
 * ```
 * $builder        
 *      ->add('duration', DateIntervalType::class, array(
 *          'unit_choices' => [
 *              'Years' => 'Y',
 *              'Months' => 'M',
 *      ]
 * ));
 * ```
 *
 */
class DateIntervalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('n', IntegerType::class, [
                'scale' => 0,
                'constraints' => [
                    new GreaterThan([
                        'value' => 0
                    ])
                ]
            ])
            ->add('unit', ChoiceType::class, [
                'choices' => $options['unit_choices'],
                'choices_as_values' => true
            ])
            ;
        
        $builder->addModelTransformer(new DateIntervalTransformer());
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefined('unit_choices')
            ->setDefault('unit_choices', [
                'Days' => 'D',
                'Weeks' => 'W',
                'Months' => 'M',
                'Years' => 'Y'
            ])
            ->setAllowedValues('unit_choices', function($values) {
                if (FALSE === is_array($values)) {
                    throw new InvalidOptionsException("The value `unit_choice` should be an array");
                }
                
                $diff = \array_diff(\array_values($values), ['D', 'W', 'M', 'Y']);
                if (count($diff) == 0) {
                    return true;
                } else {
                    throw new InvalidOptionsException(sprintf("The values of the "
                        . "units should be 'D', 'W', 'M', 'Y', those are invalid: %s",
                        \implode(', ', $diff)));
                }
            })
            ;
    }

}
