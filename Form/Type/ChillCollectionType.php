<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;

/**
 * Available options :
 * 
 * - `button_add_label`
 * - `button_remove_label`
 * - `identifier`: an identifier to identify the kind of collecton. Useful if some
 *  javascript should be launched associated to `add_entry`, `remove_entry` events.
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ChillCollectionType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'button_add_label' => 'Add an entry',
                'button_remove_label' => 'Remove entry',
                'identifier' => ''
            ]);
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['button_add_label'] = $options['button_add_label'];
        $view->vars['button_remove_label'] = $options['button_remove_label'];
        $view->vars['allow_delete'] = (int) $options['allow_delete'];
        $view->vars['allow_add'] = (int) $options['allow_add'];
        $view->vars['identifier'] = $options['identifier'];
    }
    
    public function getParent()
    {
        return \Symfony\Component\Form\Extension\Core\Type\CollectionType::class;
    }
}
