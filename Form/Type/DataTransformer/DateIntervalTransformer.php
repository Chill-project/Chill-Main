<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\Type\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class DateIntervalTransformer implements DataTransformerInterface
{
    /**
     * 
     * @param \DateInterval $value
     * @throws UnexpectedTypeException
     */
    public function transform($value)
    {
        if (empty($value)) {
            return null;
        }
        
        if (!$value instanceof \DateInterval) {
            throw new UnexpectedTypeException($value, \DateInterval::class);
        }
        
        if ($value->d > 0) {
            // we check for weeks (weeks are converted to 7 days)
            if ($value->d % 7 === 0) {
                return [
                    'n' => $value->d / 7,
                    'unit' => 'W'
                    ];
            } else {
                return [
                    'n' => $value->d,
                    'unit' => 'D'
                ];
            }
        } elseif ($value->m > 0) {
            return [
                'n' => $value->m,
                'unit' => 'M'
            ];
        } elseif ($value->y > 0) {
            return [
                'n' => $value->y,
                'unit' => 'Y'
            ];
        }
        
        throw new TransformationFailedException('the date interval does not '
            . 'contains any days, months or years');
    }

    public function reverseTransform($value)
    {
        if (empty($value) or empty($value['n'])) {
            return null;
        }
        
        $string = 'P'.$value['n'].$value['unit'];
        
        try {
            return new \DateInterval($string);
        } catch (\Exception $e) {
            throw new TransformationFailedException("Could not transform value "
                . "into DateInterval", 1542, $e);
        }
        
        
    }
}
