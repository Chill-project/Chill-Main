<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Role\Role;


/**
 * Pick a user available for the given role and center.
 *
 * Options :
 *
 * - `role`  : the role the user can reach
 * - `center`: the center a user can reach
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserPickerType extends AbstractType
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     *
     * @var \Chill\MainBundle\Repository\UserRepository
     */
    protected $userRepository;

    public function __construct(
        AuthorizationHelper $authorizationHelper,
        TokenStorageInterface $tokenStorage,
        EntityRepository $userRepository
    ) {
        $this->authorizationHelper = $authorizationHelper;
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            // create `center` option
            ->setRequired('center')
            ->setAllowedTypes('center', [\Chill\MainBundle\Entity\Center::class ])
            // create ``role` option
            ->setRequired('role')
            ->setAllowedTypes('role', ['string', \Symfony\Component\Security\Core\Role\Role::class ])
            ;

        $resolver
            ->setDefault('having_permissions_group_flag', null)
            ->setAllowedTypes('having_permissions_group_flag', ['string', 'null'])
            ->setDefault('class', User::class)
            ->setDefault('placeholder', 'Choose an user')
            ->setDefault('choice_label', function(User $u) {
                return $u->getUsername();
            })
            ->setNormalizer('choices', function(Options $options) {
                
                $users = $this->authorizationHelper
                    ->findUsersReaching($options['role'], $options['center']);
                
                if (NULL !== $options['having_permissions_group_flag']) {
                    return $this->userRepository
                        ->findUsersHavingFlags($options['having_permissions_group_flag'], $users)
                        ;
                }
                
                return $users;
            })
            ;
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
