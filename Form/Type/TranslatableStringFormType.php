<?php

namespace Chill\MainBundle\Form\Type;

/*
 * TODO
 */

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TranslatableStringFormType extends AbstractType
{
    private $availableLanguages;  // The langauges availaible
    private $frameworkTranslatorFallback; // The langagues used for the translation

    public function __construct(array $availableLanguages, Translator $translator) {
        $this->availableLanguages = $availableLanguages;
        $this->frameworkTranslatorFallback = $translator->getFallbackLocales();
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        foreach ($this->availableLanguages as $lang) {
            $builder->add($lang, TextType::class,
                array('required' => (in_array($lang,
                      $this->frameworkTranslatorFallback))));
        }
    }

    public function getBlockPrefix()
    {
        return 'translatable_string';
    }
}
