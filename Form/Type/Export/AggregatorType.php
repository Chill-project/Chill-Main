<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Form\Type\Export;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;

use Chill\MainBundle\Export\ExportManager;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AggregatorType extends AbstractType
{

    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exportManager = $options['export_manager'];
        $aggregator = $exportManager->getAggregator($options['aggregator_alias']);

        $builder
                ->add('enabled', CheckboxType::class, array(
                    'value' => true,
                    'required' => false,
                    'data' => false
                ));

        $filterFormBuilder = $builder->create('form', FormType::class, array(
                    'compound' => true,
                    'required' => false,
                    'error_bubbling' => false
            ));
        $aggregator->buildForm($filterFormBuilder);

        $builder->add($filterFormBuilder);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('aggregator_alias')
                ->setRequired('export_manager')
                ->setDefault('compound', true)
                ->setDefault('error_bubbling', false)
                ;
    }

}
