<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Form\Type\Export;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Chill\MainBundle\Export\ExportManager;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Chill\MainBundle\Export\ExportElementWithValidationInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;

/**
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class FilterType extends AbstractType
{
    const ENABLED_FIELD = 'enabled';

    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $exportManager = $options['export_manager'];
        $filter = $exportManager->getFilter($options['filter_alias']);

        $builder
                ->add(self::ENABLED_FIELD, CheckboxType::class, array(
                    'value' => true,
                    'data' => false,
                    'required' => false
                ));

        $filterFormBuilder = $builder->create('form', FormType::class, array(
            'compound' => true,
            'error_bubbling' => false,
            'required' => false,
            ));
        $filter->buildForm($filterFormBuilder);

        $builder->add($filterFormBuilder);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('filter_alias')
                ->setRequired('export_manager')
                ->setDefault('compound', true)
                ->setDefault('error_bubbling', false)
                ;
    }

}
