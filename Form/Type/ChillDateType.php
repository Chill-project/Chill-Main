<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Display the date in a date picker.
 * 
 * Extends the symfony `Symfony\Component\Form\Extension\Core\Type\DateType`
 * to automatically create a date picker.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ChillDateType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('widget', 'single_text')
            ->setDefault('attr', [ 'class' => 'datepicker' ])
            ->setDefault('format', 'dd-MM-yyyy')
            ;
    }
    
    public function getParent()
    {
        return DateType::class;
    }
}
