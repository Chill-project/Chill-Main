<?php

namespace Chill\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Psr\Log\LoggerInterface;

class UserPasswordType extends AbstractType
{
    /**
     *
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $chillLogger;
    
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder, 
        LoggerInterface $chillLogger
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->chillLogger = $chillLogger;
    }

    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('new_password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'options' => array(),
                'first_options' => array(
                    'label' => 'Password'
                ),
                'second_options' => array(
                    'label' => 'Repeat the password'
                ),
                'invalid_message' => "The password fields must match",
                'constraints' => array(
                    new Length(array(
                        'min' => 9,
                        'minMessage' => 'The password must be greater than {{ limit }} characters'
                        )),
                    new NotBlank(),
                    new Regex(array(
                        'pattern' => "/((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!,;:+\"'-\/{}~=µ\(\)£]).{6,})/",
                        'message' => "The password must contains one letter, one "
                        . "capitalized letter, one number and one special character "
                        . "as *[@#$%!,;:+\"'-/{}~=µ()£]). Other characters are allowed."
                        ))
                )
            ))
            ->add('actual_password', PasswordType::class, [
                'label' => 'Your actual password',
                'mapped' => false,
                'constraints' => [
                    new Callback([
                        'callback' => function($password, ExecutionContextInterface $context, $payload) use ($options) {
                            if (TRUE === $this->passwordEncoder->isPasswordValid($options['user'], $password)) {
                                return;
                            }
                            
                            // password problem :-)
                            $this->chillLogger
                                ->notice("incorrect password when trying to change password", [
                                    'username' => $options['user']->getUsername()
                                ]);
                            $context->addViolation('Incorrect password');
                        }
                    ])
                ]
            ])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('user')
            ->setAllowedTypes('user', \Chill\MainBundle\Entity\User::class)
            ;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_mainbundle_user_password';
    }
}
