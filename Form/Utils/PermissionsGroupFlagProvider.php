<?php


namespace Chill\MainBundle\Form\Utils;

/**
 *
 * 
 */
interface PermissionsGroupFlagProvider
{
    /**
     * Return an array of flags
     * 
     * @return string[] an array. Keys are ignored.
     */
    public function getPermissionsGroupFlags(): array;
}
