<?php

namespace Chill\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Chill\MainBundle\Form\Utils\PermissionsGroupFlagProvider;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PermissionsGroupType extends AbstractType
{
    /**
     *
     * @var PermissionsGroupFlagProvider[]
     */
    protected $flagProviders = [];
    
    const FLAG_SCOPE = 'permissions_group';
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
        ;
        
        $flags = $this->getFlags();
        
        if (count($flags) > 0) {
            $builder
                ->add('flags', ChoiceType::class, [
                    'choices' => \array_combine($flags, $flags),
                    'multiple' => true,
                    'expanded' => true,
                    'required' => false
                ]);
        }
    }
    
    /**
     * 
     * @return array
     */
    protected function getFlags(): array
    {
        $flags = [];
        
        foreach ($this->flagProviders as $flagProvider) {
            $flags = \array_merge($flags, $flagProvider->getPermissionsGroupFlags());
        }
        
        return $flags;
    }
    
    public function addFlagProvider(PermissionsGroupFlagProvider $provider)
    {
        $this->flagProviders[] = $provider;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\MainBundle\Entity\PermissionsGroup'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_mainbundle_permissionsgroup';
    }
}
