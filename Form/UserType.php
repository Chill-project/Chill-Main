<?php

namespace Chill\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use Chill\MainBundle\Form\UserPasswordType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ;
        if ($options['is_creation']) {
            $builder->add('plainPassword', RepeatedType::class, array(
                'mapped' => false,
                'type' => PasswordType::class,
                'required' => false,
                'options' => array(),
                'first_options' => array(
                    'label' => 'Password'
                ),
                'second_options' => array(
                    'label' => 'Repeat the password'
                ),
                'invalid_message' => "The password fields must match",
                'constraints' => array(
                    new Length(array(
                        'min' => 9,
                        'minMessage' => 'The password must be greater than {{ limit }} characters'
                        )),
                    new NotBlank(),
                    new Regex(array(
                        'pattern' => "/((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!,;:+\"'-\/{}~=µ\(\)£]).{6,})/",
                        'message' => "The password must contains one letter, one "
                        . "capitalized letter, one number and one special character "
                        . "as *[@#$%!,;:+\"'-/{}~=µ()£]). Other characters are allowed."
                        ))
                )
            ));

        } else {
            $builder->add($builder
                    ->create('enabled', ChoiceType::class, array(
                        'choices' => array(
                            'Disabled, the user is not allowed to login' => 0,
                            'Enabled, the user is active' => 1
                        ),
                        'expanded' => false,
                        'multiple' => false,
                        'choices_as_values' => true // Can be removed when upgraded to Sf3.
                        ))
                );
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\MainBundle\Entity\User'
        ));

        $resolver
                ->setDefaults(array('is_creation' => false))
                ->addAllowedValues('is_creation', array(true, false))
                ;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'chill_mainbundle_user';
    }
}
