<?php
/*
 * Copyright (C) 2018 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Form\ChoiceLoader;

use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;
use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Chill\MainBundle\Repository\PostalCodeRepository;
use Symfony\Component\Form\ChoiceList\LazyChoiceList;
use Chill\MainBundle\Entity\PostalCode;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PostalCodeChoiceLoader implements ChoiceLoaderInterface
{
    /**
     *
     * @var PostalCodeRepository
     */
    protected $postalCodeRepository;
    
    protected $lazyLoadedPostalCodes = [];
    
    public function __construct(PostalCodeRepository $postalCodeRepository)
    {
        $this->postalCodeRepository = $postalCodeRepository;
    }

    public function loadChoiceList($value = null): ChoiceListInterface
    {
        $list = new \Symfony\Component\Form\ChoiceList\ArrayChoiceList(
            $this->lazyLoadedPostalCodes, 
            function(PostalCode $pc = null) use ($value) {
                return \call_user_func($value, $pc);
            });
        
        return $list;
    }

    public function loadChoicesForValues(array $values, $value = null)
    {
        $choices = [];
        
        foreach($values as $value) {
            if (empty($value)) {
                $choices[] = null;
            } else {
                $choices[] = $this->postalCodeRepository->find($value);
            }
        }

        return $choices;
    }
    
    public function loadValuesForChoices(array $choices, $value = null)
    {
        $values = [];
        
        foreach ($choices as $choice) {
            if (NULL === $choice) {
                $values[] = null;
                continue;
            }
            
            $id = \call_user_func($value, $choice);
            $values[] = $id;
            $this->lazyLoadedPostalCodes[$id] = $choice;
        }
        
        return $values;
    }
}
