<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\DependencyInjection\Widget\Factory;

use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Allow to easily create WidgetFactory. 
 * 
 * Extending this factory, the widget will be created using the already defined
 * service created "as other services" in your configuration (the most frequent
 * way is using `services.yml` file.
 * 
 * If you need to create different service based upon place, position or 
 * definition, you should implements directly
 * `Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface`
 *
 *        
 */
abstract class AbstractWidgetFactory implements WidgetFactoryInterface
{

    /**
     *
     * {@inheritdoc}
     * 
     * Will create the definition by returning the definition from the `services.yml`
     * file (or `services.xml` or `what-you-want.yml`).
     *
     * @see \Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface::createDefinition()
     */
    public function createDefinition(ContainerBuilder $containerBuilder, $place, $order, array $config)
    {
        return $containerBuilder->getDefinition($this
                ->getServiceId($containerBuilder, $place, $order, $config)
                );
    }

}