<?php

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Chill\MainBundle\DependencyInjection\Widget\AddWidgetConfigurationTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * Configure the main bundle
 */
class Configuration implements ConfigurationInterface
{
    
    use AddWidgetConfigurationTrait;
    
    /**
     *
     * @var ContainerBuilder
     */
    private $containerBuilder;

    
    public function __construct(array $widgetFactories = array(), 
            ContainerBuilder $containerBuilder)
    {
        $this->setWidgetFactories($widgetFactories);
        $this->containerBuilder = $containerBuilder;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_main');

        $rootNode
            ->children()
                ->scalarNode('installation_name')
                    ->cannotBeEmpty()
                    ->defaultValue('Chill')
                ->end() // end of scalar 'installation_name'
                ->arrayNode('available_languages')
                    ->defaultValue(array('fr'))
                    ->prototype('scalar')->end()
                ->end() // end of array 'available_languages'
                ->arrayNode('routing')
                    ->children()
                        ->arrayNode('resources')
                        ->prototype('scalar')->end()
                        ->end() // end of array 'resources'
                    ->end() // end of children
                ->end() // end of array node 'routing'
                ->arrayNode('pagination')
                    ->canBeDisabled()
                    ->children()
                        ->integerNode('item_per_page')
                        ->info('The number of item to show in the page result, by default')
                        ->min(1)
                        ->defaultValue(50)
                        ->end() // end of integer 'item_per_page'
                    ->end() // end of children
                ->end() // end of pagination
                ->arrayNode('notifications')
                    ->children()
                        ->scalarNode('from_email')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('from_name')
                            ->cannotBeEmpty()
                        ->end()
                        ->enumNode('scheme')
                            ->cannotBeEmpty()
                            ->values(['http', 'https'])
                            ->defaultValue('https')
                        ->end()
                        ->scalarNode('host')
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end() // end of notifications
                ->arrayNode('phone_helper')
                    ->canBeUnset()
                    ->children()
                        ->scalarNode('twilio_sid')
                            ->defaultNull()
                        ->end()
                        ->scalarNode('twilio_secret')
                            ->defaultNull()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('redis')
                    ->children()
                        ->scalarNode('host')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('port')
                            ->defaultValue(6379)
                        ->end()
                        ->scalarNode('timeout')
                            ->defaultValue(1)
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('widgets')
                    ->canBeEnabled()
                    ->canBeUnset()
                    ->children()
                         ->append($this->addWidgetsConfiguration('homepage', $this->containerBuilder))
                    ->end() // end of widgets/children
                ->end() // end of widgets
                ->arrayNode('cruds')
                    ->defaultValue([])
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('class')->cannotBeEmpty()->isRequired()->end()
                            ->scalarNode('controller')
                                ->cannotBeEmpty()
                                ->defaultValue(\Chill\MainBundle\CRUD\Controller\CRUDController::class)
                            ->end()
                            ->scalarNode('name')->cannotBeEmpty()->isRequired()->end()
                            ->scalarNode('base_path')->cannotBeEmpty()->isRequired()->end()
                            ->scalarNode('base_role')->defaultNull()->end()
                            ->scalarNode('form_class')->defaultNull()->end()
                            ->arrayNode('actions')
                                ->defaultValue([
                                    'edit' => [],
                                    'new'  => []
                                ])
                                ->useAttributeAsKey('name')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('controller_action')
                                            ->defaultNull()
                                            ->info('the method name to call in the route. Will be set to the action name if left empty.')
                                            ->example("'action'")
                                        ->end()
                                        ->scalarNode('path')
                                            ->defaultNull()
                                            ->info('the path that will be **appended** after the base path. Do not forget to add '
                                                . 'arguments for the method. Will be set to the action name, including an `{id}` '
                                                . 'parameter if left empty.')
                                            ->example('/{id}/my-action')
                                        ->end()
                                        ->arrayNode('requirements')
                                            ->ignoreExtraKeys(false)
                                            ->info('the requirements for the route. Will be set to `[ \'id\' => \'\d+\' ]` if left empty.')
                                        ->end()
                                        ->scalarNode('role')
                                            ->defaultNull()
                                            ->info('the role that will be required for this action. Override option `base_role`')
                                        ->end()
                                        ->scalarNode('template')
                                            ->defaultNull()
                                            ->info('the template to render the view')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()

                ->end()
               ->end() // end of root/children
            ->end() // end of root
        ;

        
        return $treeBuilder;
    }
}
