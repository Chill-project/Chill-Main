<?php

/*
 * Copyright (C) 2014-2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Chill\MainBundle\DependencyInjection\Configuration;
use Chill\MainBundle\Doctrine\DQL\GetJsonFieldByKey;
use Chill\MainBundle\Doctrine\DQL\Unaccent;
use Chill\MainBundle\Doctrine\DQL\JsonAggregate;
use Chill\MainBundle\Doctrine\DQL\JsonbExistsInArray;
use Chill\MainBundle\Doctrine\DQL\Similarity;
use Chill\MainBundle\Doctrine\DQL\OverlapsI;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Chill\MainBundle\Doctrine\DQL\Replace;

/**
 * This class load config for chillMainExtension.
 */
class ChillMainExtension extends Extension implements PrependExtensionInterface,
    Widget\HasWidgetFactoriesExtensionInterface
{
    /**
     * widget factory
     * 
     * @var WidgetFactoryInterface[]
     */
    protected $widgetFactories = array();
    
    public function addWidgetFactory(WidgetFactoryInterface $factory)
    {
        $this->widgetFactories[] = $factory;
    }
    
    /**
     * 
     * @return WidgetFactoryInterface[]
     */
    public function getWidgetFactories()
    {
        return $this->widgetFactories;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // configuration for main bundle
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);
        
        $container->setParameter('chill_main.installation_name',
            $config['installation_name']);

        $container->setParameter('chill_main.available_languages',
            $config['available_languages']);
        
        $container->setParameter('chill_main.routing.resources', 
            $config['routing']['resources']); 
        
        $container->setParameter('chill_main.pagination.item_per_page',
            $config['pagination']['item_per_page']);
        
        $container->setParameter('chill_main.notifications', 
            $config['notifications']);
        
        $container->setParameter('chill_main.redis', 
            $config['redis']);
        
        $container->setParameter('chill_main.phone_helper', 
            $config['phone_helper'] ?? []);
        
        // add the key 'widget' without the key 'enable'
        $container->setParameter('chill_main.widgets', 
            isset($config['widgets']['homepage']) ? 
                array('homepage' => $config['widgets']['homepage']):
                array()
                );

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/logger.yml');
        $loader->load('services/repositories.yml');
        $loader->load('services/pagination.yml');
        $loader->load('services/export.yml');
        $loader->load('services/form.yml');
        $loader->load('services/validator.yml');
        $loader->load('services/widget.yml');
        $loader->load('services/controller.yml'); 
        $loader->load('services/routing.yml');
        $loader->load('services/fixtures.yml');
        $loader->load('services/menu.yml');
        $loader->load('services/security.yml');
        $loader->load('services/notification.yml');
        $loader->load('services/redis.yml');
        $loader->load('services/command.yml');
        $loader->load('services/phonenumber.yml');
        $loader->load('services/cache.yml');
        $loader->load('services/templating.yml');
                
        $this->configureCruds($container, $config['cruds'], $loader);
    }
    
    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return new Configuration($this->widgetFactories, $container);
    }
    
    
    public function prepend(ContainerBuilder $container) 
    {
        $bundles = $container->getParameter('kernel.bundles');
        //add ChillMain to assetic-enabled bundles
        if (!isset($bundles['AsseticBundle'])) {
            throw new MissingBundleException('AsseticBundle');
        }

        $asseticConfig = $container->getExtensionConfig('assetic');
        $asseticConfig['bundles'][] = 'ChillMainBundle';
        $container->prependExtensionConfig('assetic', 
                array('bundles' => array('ChillMainBundle')));
        
        //add installation_name and date_format to globals
        $chillMainConfig = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration($this
                ->getConfiguration($chillMainConfig, $container), $chillMainConfig);
        $twigConfig = array(
            'globals' => array(
                'installation' => array(
                    'name' => $config['installation_name']),
                'available_languages' => $config['available_languages']
            ),
            'form_themes' => array('ChillMainBundle:Form:fields.html.twig')
        );
        $container->prependExtensionConfig('twig', $twigConfig);
        
        //add DQL function to ORM (default entity_manager)
        $container->prependExtensionConfig('doctrine', array(
           'orm' => array(
              'dql' => array(
                 'string_functions' => array(
                    'unaccent' => Unaccent::class,
                    'GET_JSON_FIELD_BY_KEY' => GetJsonFieldByKey::class,
                    'AGGREGATE' => JsonAggregate::class,
                    'REPLACE' => Replace::class,
                 ),
                 'numeric_functions' => [
                    'JSONB_EXISTS_IN_ARRAY' => JsonbExistsInArray::class,
                    'SIMILARITY' => Similarity::class,
                    'OVERLAPSI' => OverlapsI::class
                ]
              )
           )
        ));
        
        //add dbal types (default entity_manager)
        $container->prependExtensionConfig('doctrine', array(
           'dbal' => [
               'types' => [
                   'dateinterval' => \Chill\MainBundle\Doctrine\Type\NativeDateIntervalType::class
               ]
           ]
        ));
        
        //add current route to chill main
        $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillMainBundle/Resources/config/routing.yml'
              )
              
           )
        ));
        
        //add a channel to log app events
        $container->prependExtensionConfig('monolog', array(
            'channels' => array('chill')
        ));
    }
    
    /**
     * 
     * @param ContainerBuilder $container
     * @param array $config the config under 'cruds' key
     * @return null
     */
    protected function configureCruds(ContainerBuilder $container, $config, Loader\YamlFileLoader $loader)
    {
        if (count($config) === 0) {
            return;
        }
        
        $loader->load('services/crud.yml');
        
        $container->setParameter('chill_main_crud_route_loader_config', $config);
        
        $definition = new Definition();
        $definition
            ->setClass(\Chill\MainBundle\CRUD\Routing\CRUDRoutesLoader::class)
            ->addArgument('%chill_main_crud_route_loader_config%')
            ;
        
        $container->setDefinition('chill_main_crud_route_loader', $definition);
        
        $alreadyExistingNames = [];
        
        foreach ($config as $crudEntry) {
            $controller = $crudEntry['controller'];
            $controllerServiceName = 'cscrud_'.$crudEntry['name'].'_controller';
            $name = $crudEntry['name'];
            
            // check for existing crud names
            if (\in_array($name, $alreadyExistingNames)) {
                throw new LogicException(sprintf("the name %s is defined twice in CRUD", $name));
            }
            
            if (!$container->has($controllerServiceName)) {
                $controllerDefinition = new Definition($controller);
                $controllerDefinition->addTag('controller.service_arguments');
                $controllerDefinition->setAutoconfigured(true);
                $controllerDefinition->setClass($crudEntry['controller']);
                $container->setDefinition($controllerServiceName, $controllerDefinition);
            }
            
            $container->setParameter('chill_main_crud_config_'.$name, $crudEntry);
            $container->getDefinition($controllerServiceName)
                ->addMethodCall('setCrudConfig', ['%chill_main_crud_config_'.$name.'%']);
        }
    }
}
