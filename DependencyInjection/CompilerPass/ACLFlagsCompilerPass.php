<?php
/*
 */
namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Chill\MainBundle\Form\PermissionsGroupType;
use Symfony\Component\DependencyInjection\Reference;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ACLFlagsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $permissionGroupType = $container->getDefinition(PermissionsGroupType::class);
        
        foreach($container->findTaggedServiceIds('chill_main.flags') as $id => $tags) {
            $reference = new Reference($id);
            
            foreach ($tags as $tag) {
                switch($tag['scope']) {
                    case PermissionsGroupType::FLAG_SCOPE:
                        
                        $permissionGroupType->addMethodCall('addFlagProvider', [ $reference ]);
                        break;
                    default:
                        throw new \LogicalException(sprintf(
                            "This tag 'scope' is not implemented: %s, on service with id %s", $tag['scope'], $id)
                            );
                }
            }
        }
    }
}
