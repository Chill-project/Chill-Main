<?php
/*
 * Copyright (C) 2019 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use Chill\MainBundle\Form\Type\Export\PickCenterType;

/**
 * 
 *
 * 
 */
class GroupingCenterCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (FALSE === $container->hasDefinition('chill.main.form.pick_centers_type')) {
            throw new \LogicException("The service chill.main.form.pick_centers_type does "
                . "not exists in container");
        }
        
        $pickCenterType = $container->getDefinition('chill.main.form.pick_centers_type');
        
        foreach ($container->findTaggedServiceIds('chill.grouping_center') as $serviceId => $tagged) {
            $pickCenterType->addMethodCall('addGroupingCenter', 
                [ new Reference($serviceId) ]);
        }
    }
}
