<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use Chill\MainBundle\Routing\MenuComposer;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MenuCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('chill.main.menu_composer')) { 
            throw new \LogicException(sprintf("The service %s does not exists in "
                . "container.", MenuComposer::class));
        }
        
        $menuComposerDefinition = $container->getDefinition('chill.main.menu_composer'); 
        
        foreach ($container->findTaggedServiceIds('chill.menu_builder') as $id => $tags) {
            $class = $container->getDefinition($id)->getClass();
            foreach ($class::getMenuIds() as $menuId) {
                $menuComposerDefinition
                    ->addMethodCall('addLocalMenuBuilder', [new Reference($id), $menuId]);
            }
        }
    }
}
