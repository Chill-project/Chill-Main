<?php
/*
 * Copyright (C) 2019 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Phonenumber;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Chill\MainBundle\Phonenumber\PhonenumberHelper;

/**
 * 
 *
 */
class Templating extends AbstractExtension
{
    /**
     *
     * @var PhonenumberHelper
     */
    protected $phonenumberHelper;
    
    public function __construct(PhonenumberHelper $phonenumberHelper)
    {
        $this->phonenumberHelper = $phonenumberHelper;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('chill_format_phonenumber', [$this, 'formatPhonenumber'])
        ];
    }
    
    public function formatPhonenumber($phonenumber)
    {
        return $this->phonenumberHelper->format($phonenumber) ?? $phonenumber;
    }
}
