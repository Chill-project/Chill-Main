<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Prepare a client authenticated with a user 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
trait PrepareClientTrait
{
    /**
     * Create a new client with authentication information.
     * 
     * @param string $username the username (default 'center a_social')
     * @param string $password the password (default 'password')
     * @return \Symfony\Component\BrowserKit\Client
     * @throws \LogicException
     */
    public function getClient(
        $username = 'center a_social', 
        $password = 'password'
    ) {
        if (!$this instanceof WebTestCase) {
            throw new \LogicException(sprintf("The current class does not "
                . "implements %s", WebTestCase::class));
        }
        
        return static::createClient(array(), array(
           'PHP_AUTH_USER' => $username,
           'PHP_AUTH_PW'   => $password,
        ));
    }
}
