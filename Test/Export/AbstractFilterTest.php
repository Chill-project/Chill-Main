<?php
/*
 * Copyright (C) 2016 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Test\Export;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 *  Helper to test filters
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
abstract class AbstractFilterTest extends KernelTestCase
{
    /**
     *
     * @var \Prophecy\Prophet
     */
    protected $prophet;
    
    /**
     * Create a filter which will be used in tests.
     * 
     * This method is always used after an eventuel `setUp` method.
     * 
     * @return \Chill\MainBundle\Export\FilterInterface
     */
    abstract public function getFilter();
    
    /**
     * Create possible combinaison of data (produced by the form).
     * 
     * This data will be used to generate data providers using this data.
     * 
     * As all data providers, this method is executed **before** calling
     * the `setUp` method.
     * 
     * @return array an array of data. Example : `array( array(), array('fields' => array(1,2,3), ...)` where an empty array and `array(1,2,3)` are possible values
     */
    public abstract function getFormData();
    
    /**
     * Return an array with different minimal query builders
     * 
     * As all data providers, this method is executed **before** calling
     * the `setUp` method.
     * 
     * @return QueryBuilder[] an array of query builder
     */
    public abstract function getQueryBuilders();
    
    public function dataProviderAlterQuery()
    {
        foreach ($this->getQueryBuilders() as $qb) {
            foreach ($this->getFormData() as $data) {
                yield  array($qb, $data);
            }
        }
    }
    
    public function dataProviderDescriptionAction()
    {
        foreach ($this->getFormData() as $data) {
            yield array($data);
        }
    }
    
    public function setUp()
    {
        $this->prepareProphet();
    }
    
    
    public function testApplyOn()
    {
        $filter = $this->getFilter();
        
        $this->assertInternalType('string', $filter->applyOn());
    }
    
    /**
     * test the alteration of query by the filter
     * 
     * @dataProvider dataProviderAlterQuery
     * @param QueryBuilder $query
     * @param type $data
     */
    public function testAlterQuery(QueryBuilder $query, $data)
    {
        // retains informations about query
        $nbOfFrom   = $query->getDQLPart('from') !== null ? 
            count($query->getDQLPart('from')) : 0;
        $nbOfWhere  = $query->getDQLPart('where') !== null ? 
            $query->getDQLPart('where')->count() : 0;
        $nbOfSelect = $query->getDQLPart('select') !== null ?
            count($query->getDQLPart('select')) : 0;
        
        $this->getFilter()->alterQuery($query, $data);
        
        $this->assertGreaterThanOrEqual(
            $nbOfFrom, 
            $query->getDQLPart('from') !== null ? count($query->getDQLPart('from')) : 0,
            "Test that there are equal or more 'from' clause after that the filter has 
                altered the query"
            );
        $this->assertGreaterThanOrEqual(
            $nbOfWhere, 
            $query->getDQLPart('where') !== null ? $query->getDQLPart('where')->count() : 0,
            "Test that there are equal or more 'where' clause after that the filter has"
            . "altered the query"
            );
        $this->assertEquals(
            $nbOfSelect, 
            $query->getDQLPart('select') !== null ? count($query->getDQLPart('select')) : 0,
            "Test that the filter has no altered the 'select' part of the query"
            );
        
    }
    
    public function testGetTitle()
    {
        $title = $this->getFilter()->getTitle();
        
        $this->assertInternalType('string', $title);
        $this->assertNotEmpty($title,
            "test that the title is not empty");
    }
    
    /**
     * 
     * @dataProvider dataProviderDescriptionAction
     * @param array $data
     */
    public function testDescriptionAction($data)
    {
        $description = $this->getFilter()->describeAction($data);
        
        $this->assertTrue(
            is_string($description) || is_array($description),
            "test that the description is a string or an array"
            );
        
        if (is_string($description)) {
            $this->assertNotEmpty($description,
            "test that the description is not empty");
        } elseif (is_array($description)) {
            $this->assertInternalType('string', $description[0],
                    "test that the first element in the description array is a string");
            
            // test that the message is translated
            try {
                if (static::$kernel === null) {
                    static::bootKernel();
                }
                
                $catalogue = static::$kernel->getContainer()
                    ->get('translator')
                    ->getCatalogue();
                
            } catch (\Exception $ex) {
                $this->markTestIncomplete(
                    sprintf("This test is incomplete due to %s thrown by 'translator' : %s, "
                        . "complete stack : %s", get_class($ex), $ex->getMessage(),
                        $ex->getTraceAsString()));
            }
            $this->assertTrue($catalogue->has($description[0], 
                isset($description[2]) ? $description[2] : 'messages'),
                sprintf("Test that the message returned by getDescriptionAction is "
                . "present in the catalogue of translations. HINT : check that \"%s\" "
                . "is correctly translated", $description[0]));
        }
        
    }
}
