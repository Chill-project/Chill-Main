<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Test\Export;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;

/**
 * Helper which creates a set of test for aggregators.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
abstract class AbstractAggregatorTest extends KernelTestCase
{
    /**
     * Create an aggregator instance which will be used in tests.
     * 
     * This method is always used after an eventuel `setUp` method.
     * 
     * @return \Chill\MainBundle\Export\AggregatorInterface
     */
    abstract public function getAggregator();
    
    /**
     * Create possible combinaison of data (produced by the form).
     * 
     * This data will be used to generate data providers using this data.
     * 
     * This method is executed before the `setUp` method.
     * 
     * @return array an array of data. Example : `array( array(), array('fields' => array(1,2,3), ...)` where an empty array and `array(1,2,3)` are possible values
     */
    public abstract function getFormData();
    
    /**
     * get an array of query builders that the aggregator will use.
     * 
     * Those query builders will be used to test aggregator behaviour on this
     * query builder. 
     * 
     * This method is executed before the `setUp` method.
     * 
     * @return \Doctrine\DBAL\Query\QueryBuilder[]
     */
    public abstract function getQueryBuilders();
    
    /**
     * prepare data for `testGetQueryKeys`
     */
    public function dataProviderGetQueryKeys()
    {
        foreach ($this->getFormData() as $data) {
            yield array($data);
        }
    }
    
    /**
     * prepare date for method `testGetResultsAndLabels`
     */
    public function dataProviderGetResultsAndLabels()
    {
        foreach ($this->getQueryBuilders() as $qb) {
            foreach ($this->getFormData() as $data) {
                yield array(clone $qb, $data);
            }
        }
    }
    
    /**
     * provide data for `testAlterQuery`
     */
    public function dataProviderAlterQuery()
    {
        foreach ($this->getQueryBuilders() as $qb) {
            foreach ($this->getFormData() as $data) {
                yield array(clone $qb, $data);
            }
        }
    }
    
    /**
     * Test the `applyOn` method.
     */
    public function testApplyOn()
    {
        $filter = $this->getAggregator();
        
        $this->assertInternalType('string', $filter->applyOn(),
            "test that the internal type of \"applyOn\" is a string");
        $this->assertNotEmpty($filter->applyOn(),
            "test that the \"applyOn\" method return a non-empty string");
    }
    
    /**
     * test the `getTitle` method
     */
    public function testGetTitle()
    {
        $title = $this->getAggregator()->getTitle();
        
        $this->assertInternalType('string', $title);
        $this->assertNotEmpty($title,
            "test that the title is not empty");
    }
    
    /**
     * Test that the query keys are strings
     * 
     * @param array $data
     * @dataProvider dataProviderGetQueryKeys
     */
    public function testGetQueryKeys(array $data)
    {
        $queryKeys = $this->getAggregator()->getQueryKeys($data);
        
        $this->assertInternalType('array', $queryKeys,
            "test that the query keys returned are an array");
        $this->assertContainsOnly("string", $queryKeys,
            "test that the query keys returned by `getQueryKeys` are only strings");
        $this->assertGreaterThanOrEqual(1, count($queryKeys),
            "test that there are at least one query key returned");
    }
    
    /**
     * 
     * Test that 
     * 
     * - the results have a correct form (are arrays or traversable)
     * - each key in a row are present in getQueryKeys ;
     * - each returned object of the `getLabels` method is callable
     * - each result can be converted to string using this callable
     * - each of this callable can provide a string for '_header'
     * 
     * @param QueryBuilder $qb
     * @param array $data
     * 
     * @dataProvider dataProviderGetResultsAndLabels
     */
    public function testGetResultsAndLabels(QueryBuilder $qb, array $data)
    {
        // it is more convenient to  group the `getResult` and `getLabels` test
        // due to the fact that testing both methods use the same tools.
        
        // limit the result for the query for performance reason
        $qb->setMaxResults(1);
        
        $queryKeys = $this->getAggregator()->getQueryKeys($data);
        $this->getAggregator()->alterQuery($qb, $data);
        
        $results = $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
        
        if (count($results) === 0) {
            $this->markTestIncomplete("The result is empty. We cannot process tests "
                . "on results");
        }
        
        // testing the result
        $result = $results[0];
        
        $this->assertTrue( $result instanceof \Traversable || is_array($result),
            "test that each row in the result is traversable or an array");
        
        foreach ($queryKeys as $key) {
            $this->assertContains($key, array_keys($result),
                "test that each key is present in `getQueryKeys`");
            
            $closure = $this->getAggregator()->getLabels($key, array($result[$key]), $data);
            
            $this->assertTrue(is_callable($closure, false),
                "test that the `getLabels` for key is a callable");
            $this->assertTrue(is_string((string) call_user_func($closure, $result[$key])),
                sprintf("test that the callable return by `getLabels` for key %s "
                    . "is a string or an be converted to a string", $key));
            
            $this->assertTrue(
                // conditions
                is_string((string) call_user_func($closure, '_header'))
                && !empty(call_user_func($closure, '_header'))
                && call_user_func($closure, '_header') !== '_header',
                // message
                sprintf("Test that the callable return by `getLabels` for key %s "
                . "can provide an header", $key)
                );
        }
    }
    
    /**
     * test the alteration of query by the filter
     * 
     * @dataProvider dataProviderAlterQuery
     * @param QueryBuilder $query
     * @param type $data
     */
    public function testAlterQuery(QueryBuilder $query, $data)
    {
        // retains informations about query
        $nbOfFrom   = $query->getDQLPart('from') !== null ? 
            count($query->getDQLPart('from')) : 0;
        $nbOfWhere  = $query->getDQLPart('where') !== null ? 
            $query->getDQLPart('where')->count() : 0;
        $nbOfSelect = $query->getDQLPart('select') !== null ?
            count($query->getDQLPart('select')) : 0;
        
        $this->getAggregator()->alterQuery($query, $data);
        
        $this->assertGreaterThanOrEqual(
            $nbOfFrom, 
            $query->getDQLPart('from') !== null ? count($query->getDQLPart('from')) : 0,
            "Test that there are equal or more 'from' clause after that the filter has 
                altered the query");
        $this->assertGreaterThanOrEqual(
            $nbOfWhere, 
            $query->getDQLPart('where') !== null ? $query->getDQLPart('where')->count() : 0,
            "Test that there are equal or more 'where' clause after that the filter has"
            . "altered the query");
        $this->assertGreaterThanOrEqual(
            $nbOfSelect, 
            $query->getDQLPart('select') !== null ? count($query->getDQLPart('select')) : 0,
            "Test that the filter has no altered the 'select' part of the query");
        
    }
}
