/**
 * Javascript file which handle ChillCollectionType
 * 
 * Two events are emitted by this module, both on window and on collection / ul. 
 * 
 * Collection (an UL element) and entry (a li element) are associated with those
 * events.
 * 
 * ```
 * window.addEventListener('collection-add-entry', function(e) {
 *   console.log(e.detail.collection);
 *   console.log(e.detail.entry);
 * });
 * 
 * window.addEventListener('collection-remove-entry', function(e) {
 *   console.log(e.detail.collection);
 *   console.log(e.detail.entry);
 * });
 * 
 * collection.addEventListener('collection-add-entry', function(e) {
 *   console.log(e.detail.collection);
 *   console.log(e.detail.entry);
 * });
 * 
 * collection.addEventListener('collection-remove-entry', function(e) {
 *   console.log(e.detail.collection);
 *   console.log(e.detail.entry);
 * });
 * ```
 */
require('./collection.scss');

class CollectionEvent {
    constructor(collection, entry) {
        this.collection = collection;
        this.entry = entry;
    }
}

/**
 * 
 * @param {type} button
 * @returns {handleAdd}
 */
var handleAdd = function(button) {
    var
        form_name = button.dataset.collectionAddTarget,
        prototype = button.dataset.formPrototype,
        collection = document.querySelector('ul[data-collection-name="'+form_name+'"]'),
        entry = document.createElement('li'),
        event = new CustomEvent('collection-add-entry', { detail: { collection: collection, entry: entry } }),
        counter = collection.childNodes.length,
        content
        ;
    content = prototype.replace(new RegExp('__name__', 'g'), counter);
    entry.innerHTML = content;
    entry.classList.add('chill-collection__list__entry');
    initializeRemove(collection, entry);
    collection.appendChild(entry);
    chill.initPikaday('fr');
    
    collection.dispatchEvent(event);
    window.dispatchEvent(event);
};

var initializeRemove = function(collection, entry) {
    var
        button = document.createElement('button'),
        isPersisted = entry.dataset.collectionIsPersisted,
        content = collection.dataset.collectionButtonRemoveLabel,
        allowDelete = collection.dataset.collectionAllowDelete,
        event = new CustomEvent('collection-remove-entry', { detail: { collection: collection, entry: entry } })
        ;
        
    if (allowDelete === '0' && isPersisted === '1') {
        return;
    }
    
    button.classList.add('sc-button', 'bt-delete', 'chill-collection__list__remove-entry');
    button.textContent = content;
    
    button.addEventListener('click', function(e) {
        e.preventDefault();
        entry.remove();
        collection.dispatchEvent(event);
        window.dispatchEvent(event);
    });
    
    entry.appendChild(button);
};

window.addEventListener('load', function() {
    var 
        addButtons = document.querySelectorAll("button[data-collection-add-target]"),
        collections = document.querySelectorAll("ul[data-collection-name]")
        ;
    
    for (let i = 0; i < addButtons.length; i ++) {
        let addButton = addButtons[i];
        addButton.addEventListener('click', function(e) {
            e.preventDefault();
            handleAdd(e.target);
        });
    }
    
    for (let i = 0; i < collections.length; i ++) {
        let entries = collections[i].querySelectorAll(':scope > li');
        
        for (let j = 0; j < entries.length; j ++) {
            initializeRemove(collections[i], entries[j]);
        }
    }
});



