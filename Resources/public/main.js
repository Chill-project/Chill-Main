// import jQuery
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;

const moment = require('moment');
global.moment = moment;

const pikaday = require('pikaday-jquery');

const select2 = require('select2');
global.select2 = select2;

// import js
import {chill} from './js/chill.js';
global.chill = chill;

// css
require('./sass/scratch.scss');
require('./css/chillmain.css');
require('./css/pikaday.css');
require('./js/collection/collections.js');
require('./modules/breadcrumb/index.js');
require('./modules/download-report/index.js');
//require('./css/scratch.css');
//require('./css/select2/select2.css');
require('select2/dist/css/select2.css');
require('./modules/select_interactive_loading/index.js');
require('./modules/export-list/export-list.scss');

// img
require('./img/favicon.ico');
require('./img/logo-chill-sans-slogan_white.png');
require('./img/logo-chill-outil-accompagnement_white.png');


