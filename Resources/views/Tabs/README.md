# Fonctionnalité Tabs sur Chill-Main

Version 0.2

(to be translated)

## Description générale

**Tabs** est pensé comme une fonctionnalité paramétrique, qui permet de déployer facilement un ou plusieurs groupes d'onglets, avec des options qui permettent de varier son affichage et son comportement.

Le comportement naturel, ce sont des panneaux qui sont affichés ou cachés en javascript, lorqu'on clique sur l'onglet relatif.

 

## Controller
```
// in myController.php

public function myPageAction()
{
    return $this->render('mytemplate.html.twig', [
      'tabs' => [
         'myFirstPanel' => [
            [ 'name' => "Link 1",
              'content' => "content text"
            ],
            [ 'name' => "Link 2",
              'content' => "content text for link 2"
            ]
         ],
         'mySecondPanel' => [
            [ 'name' => "Link 1",
              'content' => "content text"
            ],
            [ 'name' => "Link 2",
              'link' => "http://my.url"
            ]
         ]
      ],
      // ...
    ]);
}
```
#### Description

* On peut définir plusieurs panneaux d'onglets qui s'afficheront dans une même page ;
* Voici la description du tableau passé au template depuis le controller :

  * Au départ on rassemble tous les panneaux d'onglets dans une seule clé, nommée ici `tabs` (la variable est utilisée dans le template, mais son nom peut changer);
  * Au niveau suivant, chaque clé correspond à l'identifiant(id) d'un panneau d'onglet ;
  * Au niveau suivant, chaque onglet est défini par un tableau, qui comprends les clés suivantes :

    * la clé `name`, le titre de l'onglet ;
    * la clé `content`, le contenu du panneau relatif à l'onglet. A ce stade, si `content` contient un tableau, il vous faudra adapter la boucle twig de la macro (voir ci-dessous) ;
    * une clé `link` (facultative)

* Lorsque la clé `link` est définie pour un onglet, le comportement est différent : le lien est suivi et on quitte la page. Un panneau d'onglets peut être mixte, avec certains onglets qui sont en réalité des liens.


## Template

```
{# in mytemplate.html.twig #}

{% block css%}
    <link rel="stylesheet" href="{{ asset('build/tabs.css') }}"/>
{% endblock %}

{% block js%}
    <script type="text/javascript">

        let tabParams = [{
            id : 'myFirstPanel',
            type: 'tab',
            initPane: 2,
            disposition: 'horizontal',
            fade: true
        },
        {
            id : 'mySecondPanel',
            type: 'pill',
            initPane: 5,
            disposition: 'vertical',
            fade: false
        }
        ];

    </script>
    <script type="text/javascript" src="{{ asset('build/tabs.js') }}"></script>
{% endblock %}

{% import '@ChillMain/Tabs/macro.html.twig' as tabsPanel %}

{% block content %}

    {# Display one tabsPanel   #}
    {{ tabsPanel.displayOne(tabs, 'myFirstPanel') }}

    {# Display all tabPanels   #}
    {{ tabsPanel.display(tabs) }}

{% endblock %}
```

* Il faut commencer par charger la feuille de style `tabs.css` et le script `tabs.js` à l'intérieur des blocs css et js ;
* Avant d'appeler `tabs.js`, on définit un tableau d'options pour le script :

  * **id**: string, la clé identifiant du panneau d'onglet (obligatoire)
  * **type**: [tab|pill] affecte l'affichage (default: tab)
  * **initPane**: entier, numéro de l'onglet à afficher au chargement de la page (default: 1)
  * [prévu] **disposition**: [horizontal|vertical] affecte l'affichage (default: horizontal)
  * [prévu] **fade**: boolean, transition (default: false)
  
* Ensuite on peut importer la macro `@ChillMain/Tabs/macro.html.twig` qui permet d'afficher les panneaux d'onglets, soit un par un, soit tous les panneaux ;
* Ou on peut s'inspirer des boucles twig de la macro pour en écrire de nouvelles personnalisées ;


## Limitations
* On ne peut pas afficher plusieurs fois le même panneau dans la même page

## Aller plus loin
* fixer les options disposition et fade
* js, calculer puis fixer la hauteur du panneau suivant le plus long
* controller, avec link, on clique il charge une nouvelle page, avec le même panneau d'onglet ?!
* insérer le initPane dans les urls en paramètres GET  &initPane=xx
* quand tab.content n'est pas un string : un bloc de code html, un tableau..