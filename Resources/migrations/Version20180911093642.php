<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Fix error with recursive trigger on update
 */
final class Version20180911093642 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("DROP TRIGGER canonicalize_user_on_update ON users");
        
        $this->addSql(<<<SQL
            CREATE TRIGGER canonicalize_user_on_update
              AFTER UPDATE
              ON users
              FOR EACH ROW 
              WHEN (pg_trigger_depth() = 0)
              EXECUTE PROCEDURE canonicalize_user_on_update();
SQL
        );

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DROP TRIGGER canonicalize_user_on_update ON users");
            
        $this->addSql(<<<SQL
            CREATE TRIGGER canonicalize_user_on_update
              AFTER UPDATE
              ON users
              FOR EACH ROW 
              EXECUTE PROCEDURE canonicalize_user_on_update();
SQL
        );

    }
}
