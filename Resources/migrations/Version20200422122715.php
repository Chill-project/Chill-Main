<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add a field "isNoAddress" on addresses
 */
final class Version20200422122715 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE chill_main_address ADD isNoAddress BOOLEAN NOT NULL DEFAULT FALSE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE chill_main_address DROP isNoAddress');
    }
}
