<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Pagination;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Create paginator instances
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class PaginatorFactory
{
    /**
     * the default item per page. This may be overriden by
     * the request or inside the paginator.
     *
     * @var int
     */
    private $itemPerPage;
    
    /**
     * the router and generator for url
     *
     * @var RouterInterface
     */
    private $router;
    
    /**
     * the request stack
     *
     * @var RequestStack
     */
    private $requestStack;
    
    const DEFAULT_CURRENT_PAGE_KEY = 'page';
    const DEFAULT_ITEM_PER_NUMBER_KEY = 'item_per_page';
    const DEFAULT_PAGE_NUMBER = 1;
    
    
    public function __construct(
          RequestStack $requestStack,
          RouterInterface $router,
          $itemPerPage = 50
    ) {
        $this->itemPerPage = $itemPerPage;
        $this->requestStack = $requestStack;
        $this->router = $router;
    }
    
    /**
     * create a paginator instance
     *
     * The default route and route parameters are the current ones. If set,
     * thos route are overriden.
     *
     * @param int $totalItems
     * @param string|null $route the specific route to use in pages
     * @param array|null $routeParameters the specific route parameters to use in pages
     * @return PaginatorInterface
     */
    public function create(
          $totalItems,
          $route = null,
          array $routeParameters = null
    ) {
        
        return new Paginator(
              $totalItems,
              $this->getCurrentItemsPerPage(),
              $this->getCurrentPageNumber(),
              $route === null ? $this->getCurrentRoute() : $route,
              $routeParameters === null ? $this->getCurrentRouteParameters() :
                 $routeParameters,
              $this->router,
              self::DEFAULT_CURRENT_PAGE_KEY,
              self::DEFAULT_ITEM_PER_NUMBER_KEY);
    }
    
    /**
     *
     * @return int
     */
    public function getCurrentPageNumber()
    {
        return $this->requestStack
              ->getCurrentRequest()
              ->query
              ->getInt(self::DEFAULT_CURRENT_PAGE_KEY, self::DEFAULT_PAGE_NUMBER);
    }
    
    public function getCurrentItemsPerPage()
    {
        return $this->requestStack
              ->getCurrentRequest()
              ->query
              ->getInt(self::DEFAULT_ITEM_PER_NUMBER_KEY, $this->itemPerPage);
    }
    
    public function getCurrentPageFirstItemNumber()
    {
        return ($this->getCurrentPageNumber() - 1) *
            $this->getCurrentItemsPerPage();
    }
    
    protected function getCurrentRoute()
    {
        $request = $this->requestStack->getCurrentRequest();
        
        return $request->get('_route');
    }
    
    protected function getCurrentRouteParameters()
    {
        return array_merge(
              $this->router->getContext()->getParameters(),
              // get the route parameters
              $this->requestStack
                ->getCurrentRequest()
                ->attributes->get('_route_params'),
              // get the query parameters
              $this->requestStack
                ->getCurrentRequest()->query->all()
            );
    }
}
