<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Pagination;

/**
 * Represent a set of numbered pages
 *
 * Allow to calculate and render pagination for a set of pages.
 *
 * The items are elements that `could` be shown. The item are divided and shown
 * into pages. Each page is numbered and count a given number of item per page.
 *
 * The first page number is 1, although the first result number is 0.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface PaginatorInterface extends \Countable
{
    /**
     * get the number of results for this paginator
     *
     * @return int
     */
    public function getTotalItems();
    
    /**
     * get the first result for the current page
     *
     * @return int
     */
    public function getCurrentPageFirstItemNumber();
      
    /*
     * get the number of items per page
     */
    public function getItemsPerPage();
   
    /*
     * set the number of items per page
     */ 
    public function setItemsPerPage($itemsPerPage);
    
    /**
     * get the number of pages for this pagination.
     *
     * @return int
     */
    public function countPages();
    
    /**
     * get the current page
     *
     * @return PageInterface
     */
    public function getCurrentPage();
    
    /**
     * check if the given page is the current page
     *
     * @param \Chill\MainBundle\Pagination\PageInterface $page
     * @return bool
     */
    public function isCurrentPage(PageInterface $page);
    
    /**
     * check if the page with the given number exists
     *
     * @param int $number
     */
    public function hasPage($number);
    
    /**
     * get page by his number
     *
     * @param int $number
     * @throws \RuntimeException if the pagination has no page with specified number
     */
    public function getPage($number);
    
    /**
     * get the next page
     *
     * @return PageInterface
     * @throws \RuntimeException if the pagination has not next page
     */
    public function getNextPage();
    
    /**
     * get the previous page
     *
     * @return PageInterface
     * @throws \RuntimeException if the pagination has not previous page
     */
    public function getPreviousPage();
    
    /**
     * check if the current page has a next page
     *
     * @return bool
     */
    public function hasNextPage();
    
    /**
     * check if the current page has a page before
     *
     * @return bool
     */
    public function hasPreviousPage();
    
    /**
     * get a generator to generate pages
     *
     * @return \Generator which return PageInterface elements
     */
    public function getPagesGenerator();
    
    
}
