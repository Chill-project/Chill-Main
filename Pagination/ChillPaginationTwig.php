<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Pagination;

/**
 * add twig function to render pagination
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class ChillPaginationTwig extends \Twig_Extension
{
    const LONG_TEMPLATE = 'ChillMainBundle:Pagination:long.html.twig';
    const SHORT_TEMPLATE = 'ChillMainBundle:Pagination:short.html.twig';
    
    public function getName()
    {
        return 'chill_pagination';
    }
    
    public function getFunctions()
    {
        return array(
           new \Twig_SimpleFunction(
                 'chill_pagination',  
                 array($this, 'paginationRender'),
                 array(
                    'needs_environment' => true,
                    'is_safe' => ['html']
                 )
            )
        );
    }
    
    public function paginationRender(
          \Twig_Environment $env, 
          PaginatorInterface $paginator, 
          $template = 'ChillMainBundle:Pagination:long.html.twig'
    ) {
        switch ($template) {
            case 'long':
                $t = self::LONG_TEMPLATE;
                break;
            case 'short':
                $t = self::SHORT_TEMPLATE;
                break;
            default:
                $t = $template;
        }
        
        return $env->render($t, array(
           'paginator' => $paginator,
           'current' => $paginator->getCurrentPage()->getNumber()
        ));
    }
}
