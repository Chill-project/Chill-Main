<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2016 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Pagination;

/**
 * PageGenerator associated with a Paginator
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class PageGenerator implements \Iterator
{
    /**
     *
     * @var Paginator
     */
    protected $paginator;
    
    /**
     *
     * @var int
     */
    protected $current = 1;
    
    public function __construct(Paginator $paginator) 
    {
        $this->paginator = $paginator;;
    }
    
    public function current()
    {
        return $this->paginator->getPage($current);
    }

    public function key()
    {
        return $this->current;
    }

    public function next()
    {
        $this->current++;
    }

    public function rewind()
    {
        $this->current = 1;
    }

    public function valid()
    {
        return $this->current > 0 
              && $this->current <= $this->paginator->countPages();
    }
}
