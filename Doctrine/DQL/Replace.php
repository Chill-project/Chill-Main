<?php
/*
 * Chill is a software for social workers
 * Copyright (C) 2019 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 * 
 *
 */
class Replace extends FunctionNode
{
    protected $string;
    
    protected $from;
    
    protected $to;
    
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker): string
    {
        return 'REPLACE('.
            $this->string->dispatch($sqlWalker).
            ', '.
            $this->from->dispatch($sqlWalker).
            ', '.
            $this->to->dispatch($sqlWalker).
            ')';
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        
        $this->string = $parser->StringPrimary();
        
        $parser->match(Lexer::T_COMMA);
        
        $this->from = $parser->StringPrimary();
        
        $parser->match(Lexer::T_COMMA);
        
        $this->to = $parser->StringPrimary();
        
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
