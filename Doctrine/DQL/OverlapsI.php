<?php
/*
 * Chill is a software for social workers
 * Copyright (C) 2018 Champs-Libres Coopérative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 * DQL function for OVERLAPS function in postgresql
 * 
 * If a value is null in period start, it will be replaced by -infinity. 
 * If a value is null in period end, it will be replaced by infinity
 * 
 */
class OverlapsI extends FunctionNode
{
    private $firstPeriodStart;
    
    private $firstPeriodEnd;
    
    private $secondPeriodStart;
    
    private $secondPeriodEnd;
    
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        return '('
            .$this->makeCase($sqlWalker, $this->firstPeriodStart, 'start').', '
            .$this->makeCase($sqlWalker, $this->firstPeriodEnd, 'end').
            ') OVERLAPS ('
            .$this->makeCase($sqlWalker, $this->secondPeriodStart, 'start').', '
            .$this->makeCase($sqlWalker, $this->secondPeriodEnd, 'end').')'
            ;
    }
    
    protected function makeCase($sqlWalker, $part, $position)
    {
        //return $part->dispatch($sqlWalker);
        
        switch ($position) {
            case 'start' :
                $p = '-infinity';
                break;
            case 'end':
                $p = 'infinity';
                break;
        }
        
        if ($part instanceof \Doctrine\ORM\Query\AST\PathExpression) {
            return 'CASE WHEN '
                .'  '.$part->dispatch($sqlWalker).' IS NOT NULL '
                . 'THEN '.
                $part->dispatch($sqlWalker)
                . ' ELSE '.
                "'".$p."'::date "
                . 'END';
        } else {
            return 'CASE WHEN '
                .'  '.$part->dispatch($sqlWalker).'::date IS NOT NULL '
                . 'THEN '.
                $part->dispatch($sqlWalker)
                . '::date ELSE '.
                "'".$p."'::date "
                . 'END';
        }
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        
        $this->firstPeriodStart = $parser->StringPrimary();
        
        $parser->match(Lexer::T_COMMA);
        
        $this->firstPeriodEnd = $parser->StringPrimary();
        
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
        
        $parser->match(Lexer::T_COMMA);
        
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        
        $this->secondPeriodStart = $parser->StringPrimary();
        
        $parser->match(Lexer::T_COMMA);
        
        $this->secondPeriodEnd = $parser->StringPrimary();
        
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
