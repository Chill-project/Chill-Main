<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bridge\Twig\Extension\RoutingExtension;

/**
 * Provides function to build path with returnPath.
 * 
 * The logic of the function is based on the original routing extension.
 *
 */
class ChillTwigRoutingHelper extends AbstractExtension
{
    /**
     *
     * @var RequestStack
     */
    protected $requestStack;
    
    /**
     *
     * @var RoutingExtension
     */
    protected $originalExtension;
    
    public function __construct(
        RequestStack $requestStack, 
        RoutingExtension $originalExtension
    ) {
        $this->requestStack = $requestStack;
        $this->originalExtension = $originalExtension;
    }

    
    public function getFunctions()
    {
        return [
            new TwigFunction('chill_return_path_or', [$this, 'getReturnPathOr'], ['is_safe_callback' => [$this, 'isUrlGenerationSafe']] ),
            new TwigFunction('chill_path_add_return_path', [$this, 'getPathAddReturnPath'], ['is_safe_callback' => [$this, 'isUrlGenerationSafe']] ),
            new TwigFunction('chill_path_forward_return_path', [$this, 'getPathForwardReturnPath'], ['is_safe_callback' => [$this, 'isUrlGenerationSafe']] ),
            
        ];
    }
    
    public function getFilters()
    {
        return [
            new TwigFilter('chill_return_path_label', [$this, 'getLabelReturnPath']),
        ];
    }
    
    public function isUrlGenerationSafe(\Twig_Node $argsNode)
    {
        return $this->originalExtension->isUrlGenerationSafe($argsNode);
    }
    
    public function getLabelReturnPath($default)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        return $request->query->get('returnPathLabel', null) ?? $default;
    }
    
    /**
     * Return the return path if it exists, or generate the path if not.
     * 
     * @param string $name
     * @param array $parameters
     * @param bool $relative
     * @return string
     */
    public function getReturnPathOr($name, $parameters = [], $relative = false)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        if ($request->query->has('returnPath')) {
            return $request->query->get('returnPath');
        }
        
        return $this->originalExtension->getPath($name, $parameters, $relative);
    }
    
    /**
     * Build an url with a returnPath parameter to current page
     * 
     * @param string $name
     * @param array $parameters
     * @param bool $relative
     * @return string
     */
    public function getPathAddReturnPath($name, $parameters = [], $relative = false, $label = null)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        $parameters['returnPath'] = $request->getRequestUri();
        
        if ($label) {
            $parameters['returnPathLabel'] = $label;
        }
        
        return $this->originalExtension->getPath($name, $parameters, $relative);
    }
    
    /**
     * Build an url with a returnPath parameter to current page
     * 
     * @param string $name
     * @param array $parameters
     * @param bool $relative
     * @return string
     */
    public function getPathForwardReturnPath($name, $parameters = [], $relative = false)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        if ($request->query->has('returnPath')) {
            $parameters['returnPath'] = $request->query->get('returnPath');
        }
        
        return $this->originalExtension
            ->getPath(
                $name, 
                $parameters,
                $relative
            );
    }

}
