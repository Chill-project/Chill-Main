<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Templating\UI;

use Symfony\Component\Security\Core\User\UserInterface;
use Chill\MainBundle\Entity\User;

/**
 * Show a number of notification to user in the upper right corner
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CountNotificationUser
{
    /**
     *
     * @var NotificationCounterInterface[]
     */
    protected $counters = [];
    
    public function addNotificationCounter(NotificationCounterInterface $counter)
    {
        $this->counters[] = $counter;
    }
    
    public function getSumNotification(UserInterface $u): int
    {
        $sum = 0; 
        
        foreach ($this->counters as $counter) {
            $sum += $counter->addNotification($u);
        }
        
        return $sum;
    }
}
