<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Templating\Entity;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * 
 *
 */
class ChillEntityRenderExtension extends AbstractExtension
{
    /**
     *
     * @var ChillEntityRenderInterface 
     */
    protected $renders = [];
    
    /**
     *
     * @var ChillEntityRender
     */
    protected $defaultRender;
    
    public function __construct()
    {
        $this->defaultRender = new ChillEntityRender();
    }
    
    public function getFilters()
    {
        return [
            new TwigFilter('chill_entity_render_string', [$this, 'renderString'], [
                'is_safe' => [ 'html' ]
            ]),
            new TwigFilter('chill_entity_render_box', [$this, 'renderBox'], [
                'is_safe' => [ 'html' ]
            ])
        ];
    }
    
    public function renderString($entity, array $options = []): string
    {
        if (NULL === $entity) {
            return '';
        }
        
        return $this->getRender($entity, $options)
            ->renderString($entity, $options);
    }
    
    public function renderBox($entity, array $options = []): string
    {
        if (NULL === $entity) {
            return '';
        }
        
        return $this->getRender($entity, $options)
            ->renderBox($entity, $options);
    }
    
    public function addRender(ChillEntityRenderInterface $render)
    {
        $this->renders[] = $render;
    }
    
    protected function getRender($entity, $options): ?ChillEntityRenderInterface
    {
        foreach ($this->renders as $render) {
            if ($render->supports($entity, $options)) {
                return $render;
            }
        }
        
        return $this->defaultRender;
    }
}
