<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Templating\Entity;

/**
 * Render an entity using `__toString()`
 */
class ChillEntityRender extends AbstractChillEntityRender
{
    public function renderBox($entity, array $options): string
    {
        return $this->getDefaultOpeningBox('default').$entity
            .$this->getDefaultClosingBox();
    }

    public function renderString($entity, array $options): string
    {
        return (string) $entity;
    }

    public function supports($entity, array $options): bool
    {
        return true;
    }
}
