<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2019, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Templating\Entity;

/**
 * Interface to implement which will render an entity in template on a custom
 * manner.
 */
interface ChillEntityRenderInterface
{
    /**
     * Return true if the class support this object for the given options
     * 
     * @param type $entity
     * @param array $options
     * @return bool
     */
    public function supports($entity, array $options): bool;
    
    /**
     * Return the entity as a string.
     * 
     * Example: returning the name of a person.
     * 
     * @param object $entity
     * @param array $options
     * @return string
     */
    public function renderString($entity, array $options): string;
    
    /**
     * Return the entity in a box
     * 
     * Example: return a person inside a box:
     * 
     * ```html
     * <span class="chill-entity">
     *  <span class="chill-entity__first-name">Roger</span>
     *  <span class="chill-entity__last-name">Dupont</span>
     * </span>
     * ```
     * 
     * @param type $entity
     * @param array $options
     * @return string
     */
    public function renderBox($entity, array $options): string;
}
