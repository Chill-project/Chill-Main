<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2021, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Templating\Entity;

use Chill\MainBundle\Entity\Address;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Render entity `Address`
 * 
 * options:
 * 
 * `with_valid_from`: add validity date to the rendering
 * `has_no_address`: add information if the address "is no address" homeless
 * 
 */
class AddressRendering implements ChillEntityRenderInterface
{
    
    /**
     * 
     * @var TwigEngine
     */
    protected $twig;
    
    public function __construct(TwigEngine $twig)
    {
        $this->twig = $twig;
    }

    
    public function renderBox($entity, array $options): string
    {
        $defaultOptions = [
            'has_no_address' => false,
            'with_valid_from' => false
        ];
        
        return $this->twig->render('@ChillMain/Address/render_box_address.html.twig', 
            [
                'address' => $entity, 
                'options' => \array_merge($defaultOptions, $options)
            ]);
    }

    /**
     * 
     * @param Address $entity
     * @param array $options
     * @return string
     */
    public function renderString($entity, array $options): string
    {
        return \htmlspecialchars(
            $entity->getStreetAddress1().', '.
            $entity->getStreetAddress2().', '.
            $entity->getPostcode()->getCode().' '.
            $entity->getPostcode()->getName()
            );
    }

    public function supports($entity, array $options): bool
    {
        return $entity instanceof Address;
    }
}
