// this file loads all assets from the Chill main bundle
module.exports = function(encore, entries) {
    entries.push(__dirname + '/Resources/public/main.js');
    encore.addEntry('login', './vendor/chill-project/main/Resources/public/modules/login_page/index.js');
    encore.addEntry('tabs', './vendor/chill-project/main/Resources/public/modules/tabs/index.js');
    encore.addAliases({
        ShowHide: __dirname + '/Resources/public/modules/show_hide/',
        ChillMainSass: __dirname + '/Resources/public/sass'
    });
};
