<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security\PasswordRecover;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PasswordRecoverEventSubscriber implements EventSubscriberInterface
{
    /**
     *
     * @var PasswordRecoverLocker
     */
    protected $locker;
    
    public function __construct(PasswordRecoverLocker $locker)
    {
        $this->locker = $locker;
    }

    
    public static function getSubscribedEvents(): array
    {
        return [
            PasswordRecoverEvent::INVALID_TOKEN => [
                [ 'onInvalidToken' ]
            ],
            PasswordRecoverEvent::ASK_TOKEN_INVALID_FORM => [
                [ 'onAskTokenInvalidForm' ]
            ],
            PasswordRecoverEvent::ASK_TOKEN_SUCCESS => [
                [ 'onAskTokenSuccess']
            ]
        ];
    }
    
    public function onInvalidToken(PasswordRecoverEvent $event)
    {
        // set global lock
        $this->locker->createLock('invalid_token_global', null);
        
        // set ip lock
        if ($event->hasIp()) {
            $this->locker->createLock('invalid_token_by_ip', $event->getIp());
        }
    }
    
    public function onAskTokenInvalidForm(PasswordRecoverEvent $event)
    {
        // set global lock
        $this->locker->createLock('ask_token_invalid_form_global', null);
        
        // set ip lock
        if ($event->hasIp()) {
            $this->locker->createLock('ask_token_invalid_form_by_ip', $event->getIp());
        }
    }
    
    public function onAskTokenSuccess(PasswordRecoverEvent $event)
    {
        $this->locker->createLock('ask_token_success_by_user', $event->getUser());
    }
}
