<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security\PasswordRecover;

use Chill\MainBundle\Security\PasswordRecover\TokenManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Chill\MainBundle\Notification\Mailer;
use Chill\MainBundle\Entity\User;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class RecoverPasswordHelper
{
    /**
     *
     * @var TokenManager
     */
    protected $tokenManager;
    
    /**
     *
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
     *
     * @var Mailer
     */
    protected $mailer;
    
    protected $routeParameters;
    
    const RECOVER_PASSWORD_ROUTE = 'password_recover';
    
    public function __construct(
        TokenManager $tokenManager, 
        UrlGeneratorInterface $urlGenerator, 
        Mailer $mailer,
        array $routeParameters
    ) {
        $this->tokenManager = $tokenManager;
        $this->urlGenerator = $urlGenerator;
        $this->mailer = $mailer;
        $this->routeParameters = $routeParameters;
    }

    /**
     * 
     * @param User $user
     * @param \DateTimeInterface $expiration
     * @param bool $absolute
     * @param array $parameters additional parameters to url
     * @return string
     */
    public function generateUrl(User $user, \DateTimeInterface $expiration, $absolute = true, array $parameters = [])
    {
        
        $context = $this->urlGenerator->getContext();
        $previousHost = $context->getHost();
        $previousScheme = $context->getScheme();
        
        $context->setHost($this->routeParameters['host']);
        $context->setScheme($this->routeParameters['scheme']);
        
        $url = $this->urlGenerator->generate(
            self::RECOVER_PASSWORD_ROUTE, 
            \array_merge(
                $this->tokenManager->generate($user, $expiration),
                $parameters), 
            $absolute ? UrlGeneratorInterface::ABSOLUTE_URL : UrlGeneratorInterface::ABSOLUTE_PATH
            );
        
        // reset the host
        $context->setHost($previousHost);
        $context->setScheme($previousScheme);
        
        return $url;
    }
    
    public function sendRecoverEmail(
        User $user, 
        \DateTimeInterface $expiration, 
        $template = '@ChillMain/Password/recover_email.txt.twig', 
        array $templateParameters = [], 
        $force = false,
        array $additionalUrlParameters = [],
        $emailSubject = 'Recover your password'
    ) {
        $content = $this->mailer->renderContentToUser(
            $user, 
            $template, 
            \array_merge([
                    'user' => $user,
                    'url' => $this->generateUrl($user, $expiration, true, $additionalUrlParameters)
                ],
                $templateParameters
            ));
        
        $this->mailer->sendNotification(
            $user, 
            [ $emailSubject ], 
            [
                'text/plain' => $content,
            ], 
            null, 
            $force);
    }
}
