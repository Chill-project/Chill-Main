<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security\PasswordRecover;

use Chill\MainBundle\Entity\User;
use Psr\Log\LoggerInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TokenManager
{
    /**
     *
     * @var string
     */
    protected $secret;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    const TOKEN = 't';
    const HASH = 'h';
    const TIMESTAMP = 'ts';
    const USERNAME_CANONICAL = 'u';
    
    const TOKEN_LENGTH = 24;
    
    public function __construct($secret, LoggerInterface $logger)
    {
        $this->secret = $secret;
        $this->logger = $logger;
    }
    
    public function generate(User $user, \DateTimeInterface $expiration)
    {
        $token = \random_bytes(self::TOKEN_LENGTH);
        $username = $user->getUsernameCanonical();
        
        if (empty($username)) {
            throw new \UnexpectedValueException("username should not be empty to generate a token");
        }
        
        $timestamp = $expiration->getTimestamp();
        $hash = \hash('sha1', $token.$username.$timestamp.$this->secret);
        
        return [ 
            self::HASH => $hash, 
            self::TOKEN => \bin2hex($token),
            self::TIMESTAMP => $timestamp, 
            self::USERNAME_CANONICAL => $username
            ];
    }
    
    public function verify($hash, $token, User $user, $timestamp)
    {
        $token = \hex2bin(\trim($token));
        
        if (\strlen($token) !== self::TOKEN_LENGTH) {
            return false;
        }
        
        $username = $user->getUsernameCanonical();
        $date = \DateTimeImmutable::createFromFormat('U', $timestamp);
        
        if ($date < new \DateTime('now')) {
            
            $this->logger->info('receiving a password recover token with expired '
                . 'validity');
            
            return false;
        }
        
        $expected = \hash('sha1', $token.$username.$timestamp.$this->secret);
        
        if ($expected !== $hash) {
            return false;
        }
        
        return true;
    }
    
}
