<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security\PasswordRecover;

use Chill\MainBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PasswordRecoverEvent extends Event
{
    /**
     *
     * @var User
     */
    protected $user;
    
    /**
     *
     * @var string
     */
    protected $token;
    
    /**
     *
     * @var string
     */
    protected $ip;
    
    /**
     *
     * @var boolean
     */
    protected $safelyGenerated;
    
    const ASK_TOKEN_INVALID_FORM = 'chill_main.ask_token_invalid_form';
    const INVALID_TOKEN = 'chill_main.password_recover_invalid_token';
    const ASK_TOKEN_SUCCESS = 'chill_main.ask_token_success';
    
    /**
     * 
     * @param type $token
     * @param User $user
     * @param type $ip
     * @param bool $safelyGenerated true if generated safely (from console command, etc.)
     */
    public function __construct($token = null, User $user = null, $ip = null, bool $safelyGenerated = false)
    {
        $this->user = $user;
        $this->token = $token;
        $this->ip = $ip;
        $this->safelyGenerated = $safelyGenerated;
    }
    
    /**
     * 
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
    
    public function getIp()
    {
        return $this->ip;
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasIp(): bool
    {
        return !empty($this->ip);
    }
    
    /**
     * 
     * @return boolean
     */
    public function hasUser(): bool
    {
        return $this->user instanceof User;
    }
    
    public function isSafelyGenerated(): bool
    {
        return $this->safelyGenerated;
    }
}
