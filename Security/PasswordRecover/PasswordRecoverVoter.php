<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security\PasswordRecover;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PasswordRecoverVoter extends Voter
{
    const TRY_TOKEN = 'CHILL_PASSWORD_TRY_TOKEN';
    const ASK_TOKEN = 'CHILL_PASSWORD_ASK_TOKEN';
    
    protected $supported = [
        self::TRY_TOKEN,
        self::ASK_TOKEN
    ];
    
    /**
     *
     * @var PasswordRecoverLocker
     */
    protected $locker;
    
    /**
     *
     * @var RequestStack
     */
    protected $requestStack;
    
    public function __construct(PasswordRecoverLocker $locker, RequestStack $requestStack)
    {
        $this->locker = $locker;
        $this->requestStack = $requestStack;
    }

    
    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, $this->supported)) {
            return false;
        }
        
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::TRY_TOKEN:
                if (TRUE === $this->locker->isLocked('invalid_token_global')) {
                    return false;
                }
                
                $ip = $this->requestStack->getCurrentRequest()->getClientIp();
                if (TRUE === $this->locker->isLocked('invalid_token_by_ip', $ip)) {
                    return false;
                }
                
                return true;
            case self::ASK_TOKEN:
                if (TRUE === $this->locker->isLocked('ask_token_invalid_form_global')) {
                    return false;
                }
                
                $ip = $this->requestStack->getCurrentRequest()->getClientIp();
                if (TRUE === $this->locker->isLocked('ask_token_invalid_form_by_ip', $ip)) {
                    return false;
                }
                
                if ($subject instanceof User) {
                    if (TRUE === $this->locker->isLocked('ask_token_success_by_user', $subject)) {
                        return false;
                    }
                }
                
                return true;
                
        }
    }
}
