<?php

/*
 * Copyright (C) 2015 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Security\Authorization;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Voter for Chill software. 
 * 
 * This abstract Voter provide generic methods to handle object specific to Chill
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
abstract class AbstractChillVoter extends Voter implements ChillVoterInterface
{
    protected function supports($attribute, $subject)
    {
        @trigger_error('This voter should implements the new `supports` '
            . 'methods introduced by Symfony 3.0, and do not rely on '
            . 'getSupportedAttributes and getSupportedClasses methods.', 
            E_USER_DEPRECATED);

        return \in_array($attribute, $this->getSupportedAttributes($attribute))
            && \in_array(\get_class($subject), $this->getSupportedClasses());
    }
    
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        @trigger_error('This voter should implements the new `voteOnAttribute` '
            . 'methods introduced by Symfony 3.0, and do not rely on '
            . 'isGranted method', E_USER_DEPRECATED);
        
        return $this->isGranted($attribute, $subject, $token->getUser());
    }
    
}
