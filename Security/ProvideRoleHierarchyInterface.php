<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security;

/**
 * Give a hierarchy for the role. 
 * 
 * This hierarchy allow to sort roles, which is useful in UI
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface ProvideRoleHierarchyInterface extends ProvideRoleInterface
{
    /**
     * Return an array of roles, where keys are the hierarchy, and values
     * an array of roles.
     * 
     * Example: 
     * 
     * ```
     * [ 'Title' => [ 'CHILL_FOO_SEE', 'CHILL_FOO_UPDATE' ] ]
     * ```
     * 
     * @return array where keys are the hierarchy, and values an array of roles: `[ 'title' => [ 'CHILL_FOO_SEE', 'CHILL_FOO_UPDATE' ] ]`
     */
    public function getRolesWithHierarchy();
}
