<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Security\UserProvider;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserProvider implements UserProviderInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    
    public function loadUserByUsername($username): UserInterface
    {
        try {
            $user = $this->em->createQuery(sprintf(
                    "SELECT u FROM %s u "
                    . "WHERE u.usernameCanonical = UNACCENT(LOWER(:pattern)) "
                    . "OR "
                    . "u.emailCanonical = UNACCENT(LOWER(:pattern))", 
                    User::class))
                ->setParameter('pattern', $username)
                ->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw new UsernameNotFoundException(sprintf('Bad credentials.', $username));
        }
        
        return $user;
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException("Unsupported user class: cannot reload this user");
        }
        
        $reloadedUser = $this->em->getRepository(User::class)->find($user->getId());
        
        if (NULL === $reloadedUser) {
            throw new UsernameNotFoundException(sprintf('User with ID "%s" could not be reloaded.', $user->getId()));
        }
        
        return $reloadedUser;
    }

    public function supportsClass($class): bool
    {
        return $class === User::class;
    }
}
