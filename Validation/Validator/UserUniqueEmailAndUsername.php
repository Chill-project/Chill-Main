<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Validation\Validator;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Chill\MainBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserUniqueEmailAndUsername extends ConstraintValidator
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof User) {
            throw new \UnexpectedValueException("This validation should happens "
                . "only on class ".User::class);
        }
        
        if ($value->getId() !== null) {
            $countUsersByUsername = $this->em->createQuery(
                sprintf(
                    "SELECT COUNT(u) FROM %s u "
                    . "WHERE u.usernameCanonical = LOWER(UNACCENT(:username)) "
                    . "AND u != :user",
                    User::class)
                )
                ->setParameter('username', $value->getUsername())
                ->setParameter('user', $value)
                ->getSingleScalarResult();
        } else {
            $countUsersByUsername = $this->em->createQuery(
                sprintf(
                    "SELECT COUNT(u) FROM %s u "
                    . "WHERE u.usernameCanonical = LOWER(UNACCENT(:username)) ",
                    User::class)
                )
                ->setParameter('username', $value->getUsername())
                ->getSingleScalarResult();
        }
        
        if ($countUsersByUsername > 0) {
            $this->context
                ->buildViolation($constraint->messageDuplicateUsername)
                ->setParameters([
                    '%username%' => $value->getUsername()
                ])
                ->atPath('username')
                ->addViolation()
                ;
        }
        
        if ($value->getId() !== null) {
            $countUsersByEmail = $this->em->createQuery(
                sprintf(
                    "SELECT COUNT(u) FROM %s u "
                    . "WHERE u.emailCanonical = LOWER(UNACCENT(:email)) "
                    . "AND u != :user",
                    User::class)
                )
                ->setParameter('email', $value->getEmail())
                ->setParameter('user', $value)
                ->getSingleScalarResult();
        } else {
            $countUsersByEmail = $this->em->createQuery(
                sprintf(
                    "SELECT COUNT(u) FROM %s u "
                    . "WHERE u.emailCanonical = LOWER(UNACCENT(:email))",
                    User::class)
                )
                ->setParameter('email', $value->getEmail())
                ->getSingleScalarResult();
        }
        
        if ($countUsersByEmail > 0) {
            $this->context
                ->buildViolation($constraint->messageDuplicateEmail)
                ->setParameters([
                    '%email%' => $value->getEmail()
                ])
                ->atPath('email')
                ->addViolation()
                ;
        }
    }
}
