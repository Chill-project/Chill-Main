<?php
/*
 * Copyright (C) 2018 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Validation\Validator;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Chill\MainBundle\Phonenumber\PhonenumberHelper;
use Psr\Log\LoggerInterface;

/**
 * 
 *
 */
class ValidPhonenumber extends ConstraintValidator
{
    /**
     *
     * @var PhonenumberHelper
     */
    protected $phonenumberHelper;
    
    protected $logger;
    
    public function __construct(
        LoggerInterface $logger,
        PhonenumberHelper $phonenumberHelper
    ) {
        $this->phonenumberHelper = $phonenumberHelper;
        $this->logger = $logger;
    }
    
    /**
     * 
     * @param string $value
     * @param \Chill\MainBundle\Validation\Constraint\PhonenumberConstraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (FALSE === $this->phonenumberHelper->isPhonenumberValidationConfigured()) {
            $this->logger->debug('[phonenumber] skipping validation due to not configured helper');
            
            return;
        }
        
        if (empty($value)) {
            return;
        }
        
        switch($constraint->type) {
            case 'landline':
                $isValid = $this->phonenumberHelper->isValidPhonenumberLandOrVoip($value);
                $message = $constraint->notLandlineMessage;
                break;
            case 'mobile':
                $isValid = $this->phonenumberHelper->isValidPhonenumberMobile($value);
                $message = $constraint->notMobileMessage;
                break;
            case 'any':
                $isValid = $this->phonenumberHelper->isValidPhonenumberAny($value);
                $message = $constraint->notValidMessage;
                break;
            
            default:
                throw new \LogicException(sprintf("This type '%s' is not implemented. "
                    . "Possible values are 'mobile', 'landline' or 'any'", $constraint->type));
        }
        
        if (FALSE === $isValid) {
            $this->context->addViolation($message, [ '%phonenumber%' => $value ]);
        }
    }
}
