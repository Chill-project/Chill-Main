<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Chill\MainBundle\Validation\Validator\UserUniqueEmailAndUsername;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserUniqueEmailAndUsernameConstraint extends Constraint
{
    public $messageDuplicateUsername = "A user with the same or a close username already exists";
    public $messageDuplicateEmail = "A user with the same or a close email already exists";
    
    public function validatedBy()
    {
        return UserUniqueEmailAndUsername::class;
    }
    
    public function getTargets()
    {
        return [ self::CLASS_CONSTRAINT ];
    }
}
