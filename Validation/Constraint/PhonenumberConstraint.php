<?php
/*
 * Copyright (C) 2018 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * 
 * @Annotation
 */
class PhonenumberConstraint extends Constraint
{
    public $notMobileMessage = "This is not a mobile phonenumber";
    
    public $notLandlineMessage = "This is not a landline phonenumber";
    
    public $notValidMessage = "This is not a valid phonenumber";
    
    /**
     * The type of phone: landline (not able to receive sms) or mobile (can receive sms)
     *
     * @var string 'landline', 'mobile' or 'any'
     */
    public $type = null;
    
    public function validatedBy()
    {
        return \Chill\MainBundle\Validation\Validator\ValidPhonenumber::class;
    }
}
