<?php

namespace Chill\MainBundle\Entity;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Address
 */
class Address
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $streetAddress1 = '';

    /**
     * @var string
     */
    private $streetAddress2 = '';

    /**
     * @var \Chill\MainBundle\Entity\PostalCode
     */
    private $postcode;
    
    /**
     * Indicates when the address starts validation. Used to build an history
     * of address. By default, the current date.
     *
     * @var \DateTime
     */
    private $validFrom;
    
    /**
     * True if the address is a "no address", aka homeless person, ...
     *
     * @var bool
     */
    private $isNoAddress = false;
    
    /**
     * A list of metadata, added by customizable fields
     * 
     * @var array
     */
    private $customs = [];
    
    public function __construct()
    {
        $this->validFrom = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set streetAddress1
     *
     * @param string $streetAddress1
     *
     * @return Address
     */
    public function setStreetAddress1($streetAddress1)
    {
        $this->streetAddress1 = $streetAddress1 === NULL ? '' : $streetAddress1;

        return $this;
    }

    /**
     * Get streetAddress1
     *
     * @return string
     */
    public function getStreetAddress1()
    {
        return $this->streetAddress1;
    }

    /**
     * Set streetAddress2
     *
     * @param string $streetAddress2
     *
     * @return Address
     */
    public function setStreetAddress2($streetAddress2)
    {
        $this->streetAddress2 = $streetAddress2 === NULL ? '' : $streetAddress2;

        return $this;
    }

    /**
     * Get streetAddress2
     *
     * @return string
     */
    public function getStreetAddress2()
    {
        return $this->streetAddress2;
    }

    /**
     * Set postcode
     *
     * @param \Chill\MainBundle\Entity\PostalCode $postcode
     *
     * @return Address
     */
    public function setPostcode(\Chill\MainBundle\Entity\PostalCode $postcode = null)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return \Chill\MainBundle\Entity\PostalCode
     */
    public function getPostcode()
    {
        return $this->postcode;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * 
     * @param \DateTime $validFrom
     * @return \Chill\MainBundle\Entity\Address
     */
    public function setValidFrom(\DateTime $validFrom)
    {
        $this->validFrom = $validFrom;
        return $this;
    }
    
    /**
     * get "isNoAddress"
     * 
     * Indicate true if the address is a fake address (homeless, ...)
     * 
     * @return bool
     */
    public function getIsNoAddress(): bool
    {
        return $this->isNoAddress;
    }
    
    public function isNoAddress(): bool
    {
        return $this->getIsNoAddress();
    }

    /**
     * set Is No Address
     * 
     * Indicate true if the address is a fake address (homeless, ...)
     * 
     * @param bool $isNoAddress
     * @return $this
     */
    public function setIsNoAddress(bool $isNoAddress)
    {
        $this->isNoAddress = $isNoAddress;
        return $this;
    }
    
    /**
     * Get customs informations in the address
     * 
     * @return array
     */
    public function getCustoms(): array
    {
        return $this->customs;
    }

    /**
     * Store custom informations in the address
     * 
     * @param array $customs
     * @return $this
     */
    public function setCustoms(array $customs): self
    {
        $this->customs = $customs;
        
        return $this;
    }
        
    /**
     * Validate the address.
     * 
     * Check that:
     * 
     * * if the address is not home address:
     *     * the postal code is present
     *     * the valid from is not null
     *     * the address street 1 is greater than 2
     * 
     * @param ExecutionContextInterface $context
     * @param array $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getValidFrom() instanceof \DateTime) {
            $context
                ->buildViolation("address.date-should-be-set")
                ->atPath('validFrom')
                ->addViolation();
        }
        
        if ($this->isNoAddress()) {
            return;
        }
        
        if (empty($this->getStreetAddress1())) {
            $context
                ->buildViolation("address.street1-should-be-set")
                ->atPath('streetAddress1')
                ->addViolation();
        }
        
        if (!$this->getPostcode() instanceof PostalCode) {
            $context
                ->buildViolation("address.postcode-should-be-set")
                ->atPath('postCode')
                ->addViolation();
        }
    }

    
    public static function createFromAddress(Address $original) : Address
    {
        return (new Address())
            ->setPostcode($original->getPostcode())
            ->setStreetAddress1($original->getStreetAddress1())
            ->setStreetAddress2($original->getStreetAddress2())
            ->setValidFrom($original->getValidFrom())
            ;
    }

}

