<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Routing;

use Knp\Menu\MenuItem;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface LocalMenuBuilderInterface
{
    /**
     * return an array of menu ids
     * 
     * @internal this method is static to 1. keep all config in the class, 
     *      instead of tags arguments; 2. avoid a "supports" method, which could lead
     *      to parsing all instances to get only one or two working instance.
     */
    public static function getMenuIds(): array;
    
    public function buildMenu($menuId, MenuItem $menu, array $parameters);
}
