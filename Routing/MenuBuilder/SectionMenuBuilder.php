<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Knp\Menu\MenuItem;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Chill\MainBundle\Security\Authorization\ChillExportVoter;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class SectionMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;
    
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }
    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        $menu->addChild('Homepage', [
                'route' => 'chill_main_homepage'
            ])
            ->setExtras([
                'icons' => ['home'],
                'order' => 0
            ]);
        
        if ($this->authorizationChecker->isGranted(ChillExportVoter::EXPORT)) {
            $menu->addChild('Export Menu', [
                'route' => 'chill_main_export_index'
            ])
            ->setExtras([
                'icons' => ['upload'],
                'order' => 20
            ]);
        }
    }

    public static function getMenuIds(): array
    {
        return [ 'section' ];
    }
}
