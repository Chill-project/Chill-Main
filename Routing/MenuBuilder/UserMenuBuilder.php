<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Routing\MenuBuilder;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UserMenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
    
    public function buildMenu($menuId, \Knp\Menu\MenuItem $menu, array $parameters)
    {
        if ($this->tokenStorage->getToken()->getUser() instanceof User) {
            $menu
                ->addChild(
                    'Change password',
                    [ 'route' => 'change_my_password']
                )
                ->setExtras([
                    'order' => 99999999998
                ]);
        }
            
        $menu
            ->addChild(
                'Logout', 
                [
                    'route' => 'logout'
            ])
            ->setExtras([
                'order'=> 99999999999,
                'icon' => 'power-off'
            ]);
    }

    public static function getMenuIds(): array
    {
        return [ 'user' ];
    }
}
