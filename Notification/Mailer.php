<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Notification;

use Chill\MainBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Classe d'aide pour l'envoi de notification.
 * 
 * Héberge toutes les méthodes pour ré-écrire les URL en fonction de la langue
 * de l'utilisateur.
 *
 */
class Mailer
{
    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @var \Twig\Environment
     */
    protected $twig;
    
    /**
     *
     * @var \Swift_Mailer
     */
    protected $mailer;
    
    /**
     *
     * @var \Swift_Mailer 
     */
    protected $forcedMailer;
    
    /**
     *
     * @var RouterInterface
     */
    protected $router;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var array
     */
    protected $routeParameters;
    
    public function __construct(
        LoggerInterface $logger, 
        \Twig\Environment $twig, 
        \Swift_Mailer $mailer, 
        // due to bug https://github.com/symfony/swiftmailer-bundle/issues/127
        \Swift_Transport $mailerTransporter,
        RouterInterface $router, 
        TranslatorInterface $translator, 
        $routeParameters
    ) {
        $this->logger = $logger;
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->translator = $translator;
        $this->routeParameters = $routeParameters;
        $this->forcedMailer = new \Swift_Mailer($mailerTransporter);
    }

    
    /**
     * Envoie une notification à un utilisateur. 
     * 
     * @param \User $to
     * @param array $subject Subject of the message [ 0 => $message (required), 1 => $parameters (optional), 3 => $domain (optional) ]
     * @param array $bodies The bodies. An array where keys are the contentType and values the bodies
     * @param \callable $callback a callback to customize the message (add attachment, etc.)
     */
    public function sendNotification(
        $recipient,
        array $subject,
        array $bodies,
        callable $callback = null,
        $force = false
        )
    {
        $fromEmail = $this->routeParameters['from_email'];
        $fromName   = $this->routeParameters['from_name'];
        $to = $recipient instanceof User ? $recipient->getEmail() : $recipient;
        
        $subjectI18n = $this->translator->trans(
            $subject[0], 
            $subject[1] ?? [], 
            $subject[2] ?? null
        );
        
        $message = (new \Swift_Message($subjectI18n))
            ->setFrom($fromEmail, $fromName)
            ->setTo($to)
            ;
        
        foreach ($bodies as $contentType => $content) {
            $message->setBody($content, $contentType);
        }
        
        if ($callback !== null) {
            \call_user_func($callback, $message);
        }
        
        $this->logger->info("[notification] Sending notification", [
            'to' => $message->getTo(),
            'subject' => $message->getSubject()
        ]);
        
        $this->sendMessage($message, $force);
    }
    
    public function sendMessage(\Swift_Message $message, $force)
    {
        if ($force) {
            $this->forcedMailer->send($message);
        } else {
            $this->mailer->send($message);
        }
    }
    
    public function renderContentToUser(User $to, $template, array $parameters = array())
    {
        $context = $this->router->getContext();
        $previousHost = $context->getHost();
        $previousScheme = $context->getScheme();
        
        $context->setHost($this->routeParameters['host']);
        $context->setScheme($this->routeParameters['scheme']);
        
        $content = $this->twig->render($template, $parameters);
        
        // reset the host
        $context->setHost($previousHost);
        $context->setScheme($previousScheme);
        
        return $content;
    }
}
