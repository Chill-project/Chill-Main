<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2019, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\CRUD\Resolver;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * 
 *
 */
class Resolver
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var \Symfony\Component\PropertyAccess\PropertyAccessor
     */
    protected $propertyAccess;
    
    /**
     *
     * @var array
     */
    protected $crudConfig;
    
    /**
     * @deprecated
     */
    const ROLE_VIEW = 'role.view';
    
    /**
     * @deprecated
     */
    const ROLE_EDIT = 'role.edit';
    
    /**
     * The key to get the role necessary for the action
     */
    const ROLE = 'role';
    
    function __construct(EntityManagerInterface $em, array $crudConfig)
    {
        $this->em = $em;
        
        foreach($crudConfig as $conf) {
            $this->crudConfig[$conf['name']] = $conf;
        }
    }
    
    public function getConfigValue($key, $crudName, $action = null)
    {
        $config = $this->crudConfig[$crudName];
        
        switch ($key) {
            case self::ROLE:
                return $config['actions'][$action]['role'] ?? $this->buildDefaultRole($crudName, $action);
        }
    }
    
    public function buildDefaultRole($crudName, $action)
    {
        if (empty($this->crudConfig[$crudName]['base_role'])) {
            throw new \LogicException(sprintf("the base role is not defined. You must define "
                . "on or override %s or %s methods", __METHOD__, "getRoleFor"));
        }
        
        return \strtoupper(
            $this->crudConfig[$crudName]['base_role'].
            '_'.
            $action);
    }
    
    public function hasAction($crudName, $action)
    {
        return \array_key_exists($action, 
            $this->crudConfig[$crudName]['actions']);
    }
}
