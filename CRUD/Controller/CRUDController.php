<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2019, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\CRUD\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\QueryBuilder;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Chill\MainBundle\CRUD\Resolver\Resolver;
use Chill\MainBundle\Pagination\PaginatorInterface;
use Chill\MainBundle\CRUD\Form\CRUDDeleteEntityForm;

/**
 * 
 *
 */
class CRUDController extends AbstractController
{
    /**
     * The crud configuration 
     *
     * This configuration si defined by `chill_main['crud']`.
     *
     * @var array
     */
    protected $crudConfig;

    public function setCrudConfig(array $config)
    {
        $this->crudConfig = $config;
    }
    
    public function delete(Request $request, $id)
    {
        return $this->deleteAction('delete', $request, $id);
    }
    
    protected function deleteAction(string $action, Request $request, $id, $formClass = null)
    {
        $this->onPreDelete($action, $request, $id);
        
        $entity = $this->getEntity($action, $id, $request);
        
        $postFetch = $this->onPostFetchEntity($action, $request, $entity);
        
        if ($postFetch instanceof Response) {
            return $postFetch;
        }
        
        if (NULL === $entity) {
            throw $this->createNotFoundException(sprintf("The %s with id %s "
                . "is not found"), $this->getCrudName(), $id);
        }
        
        $response = $this->checkACL($action, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $response = $this->onPostCheckACL($action, $request, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $form = $this->createFormFor($action, $entity, $formClass);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->onFormValid($entity, $form, $request);
            $em = $this->getDoctrine()->getManager();
            
            $this->onPreRemove($action, $entity, $form, $request);
            $this->removeEntity($action, $entity, $form, $request);
            $this->onPostRemove($action, $entity, $form, $request);
            
            $this->onPreFlush($action, $entity, $form, $request);
            $em->flush();
            $this->onPostFlush($action, $entity, $form, $request);
            
            $this->addFlash('success', $this->generateFormSuccessMessage($action, $entity));
            
            $result = $this->onBeforeRedirectAfterSubmission($action, $entity, $form, $request);
            
            if ($result instanceof Response) {
                return $result;
            }
            
            return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_view', ['id' => $entity->getId()]);
            
        } elseif ($form->isSubmitted()) {
            $this->addFlash('error', $this->generateFormErrorMessage($action, $form));
        }
        
        $defaultTemplateParameters = [
            'form' => $form->createView(),
            'entity' => $entity,
            'crud_name' => $this->getCrudName()
        ];
        
        return $this->render(
            $this->getTemplateFor($action, $entity, $request), 
            $this->generateTemplateParameter($action, $entity, $request, $defaultTemplateParameters)
            );
    }
    
    protected function onPreDelete(string $action, Request $request) {}
    
    protected function onPreRemove(string $action, $entity, FormInterface $form, Request $request) {}
    
    protected function onPostRemove(string $action, $entity, FormInterface $form, Request $request) {}
    
    protected function removeEntity(string $action, $entity, FormInterface $form, Request $request)
    {
        $this->getDoctrine()
            ->getManager()
            ->remove($entity);
    }
    
    /**
     * Base method called by index action.
     * 
     * @param Request $request
     * @return type
     */
    public function index(Request $request)
    {
        return $this->indexEntityAction('index', $request);
    }
    
    /**
     * Build an index page.
     * 
     * Some steps may be overriden during this process of rendering.
     * 
     * This method:
     * 
     * 1.  Launch `onPreIndex`
     * x.  check acl. If it does return a response instance, return it
     * x.  launch `onPostCheckACL`. If it does return a response instance, return it
     * 1.  count the items, using `countEntities`
     * 2.  build a paginator element from the the number of entities ;
     * 3.  Launch `onPreIndexQuery`. If it does return a response instance, return it
     * 3.  build a query, using `queryEntities`
     * x.  fetch the results, using `getQueryResult`
     * x.  Launch `onPostIndexFetchQuery`. If it does return a response instance, return it
     * 4.  create default parameters:
     * 
     *     The default parameters are: 
     *     
     *         * entities: the list en entities ;
     *         * crud_name: the name of the crud ;
     *         * paginator: a paginator element ;
     * 5.  Launch rendering, the parameter is fetch using `getTemplateFor` 
     *     The parameters may be personnalized using `generateTemplateParameter`.
     * 
     * @param string $action
     * @param Request $request
     * @return type
     */
    protected function indexEntityAction($action, Request $request)
    {
        $this->onPreIndex($action, $request);
        
        $response = $this->checkACL($action, null);
        if ($response instanceof Response) {
            return $response;
        }
        
        $response = $this->onPostCheckACL($action, $request, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $totalItems = $this->countEntities($action, $request);
        $paginator = $this->getPaginatorFactory()->create($totalItems);
        
        $response = $this->onPreIndexBuildQuery($action, $request, $totalItems, 
            $paginator);
        
        if ($response instanceof Response) {
            return $response;
        }
        
        $query = $this->queryEntities($action, $request, $paginator);
        
        $response = $this->onPostIndexBuildQuery($action, $request, $totalItems, 
            $paginator, $query);
        
        if ($response instanceof Response) {
            return $response;
        }
        
        $entities = $this->getQueryResult($action, $request, $totalItems, $paginator, $query);
        
        $response = $this->onPostIndexFetchQuery($action, $request, $totalItems, 
            $paginator, $entities);
        
        if ($response instanceof Response) {
            return $response;
        }
        
        $defaultTemplateParameters = [
            'entities'  => $entities,
            'crud_name' => $this->getCrudName(),
            'paginator' => $paginator
        ];
        
        return $this->render(
            $this->getTemplateFor($action, $entities, $request), 
            $this->generateTemplateParameter($action, $entities, $request, $defaultTemplateParameters)
            );
    }
    
    /**
     * 
     * @param string $action
     * @param Request $request
     */
    protected function onPreIndex(string $action, Request $request) { }
    
    /**
     * method used by indexAction
     * 
     * @param string $action
     * @param Request $request
     * @param int $totalItems
     * @param PaginatorInterface $paginator
     */
    protected function onPreIndexBuildQuery(string $action, Request $request, int $totalItems, PaginatorInterface $paginator) { }
    
    /**
     * method used by indexAction
     * 
     * @param string $action
     * @param Request $request
     * @param int $totalItems
     * @param PaginatorInterface $paginator
     * @param mixed $query
     */
    protected function onPostIndexBuildQuery(string $action, Request $request, int $totalItems, PaginatorInterface $paginator, $query) { }
    
    /**
     * method used by indexAction
     * 
     * @param string $action
     * @param Request $request
     * @param int $totalItems
     * @param PaginatorInterface $paginator
     * @param mixed $entities
     */
    protected function onPostIndexFetchQuery(string $action, Request $request, int $totalItems, PaginatorInterface $paginator, $entities) { }
    
    /**
     * Build the base query for listing all entities, normally use in a listing
     * page.
     * 
     * This base query does not contains any `WHERE` or `SELECT` clauses. Those
     * are added by other methods, like `queryEntities` and `countQueries`.
     * 
     * @param string $action
     * @param Request $request
     * @return QueryBuilder
     */
    protected function buildQueryEntities(string $action, Request $request)
    {
        return $this->getDoctrine()->getManager()
            ->createQueryBuilder()
            ->select('e')
            ->from($this->getEntityClass(), 'e')
            ;
    }
    
    /**
     * Query the entity.
     * 
     * By default, get all entities.
     * 
     * The method `orderEntity` is called internally to order entities.
     * 
     * It returns, by default, a query builder.
     * 
     * @param string $action
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return type
     */
    protected function queryEntities(string $action, Request $request, PaginatorInterface $paginator)
    {
        $query = $this->buildQueryEntities($action, $request)
            ->setFirstResult($paginator->getCurrentPage()->getFirstItemNumber())
            ->setMaxResults($paginator->getItemsPerPage());
        
        // allow to order queries and return the new query
        return $this->orderQuery($action, $query, $request, $paginator);
    }
    
    
    /**
     * Add ordering fields in the query build by self::queryEntities
     * 
     * @param string $action
     * @param QueryBuilder|mixed $query by default, an instance of QueryBuilder
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return QueryBuilder|mixed
     */
    protected function orderQuery(string $action, $query, Request $request, PaginatorInterface $paginator)
    {
        return $query;
    }
    
    /**
     * Get the result of the query
     * 
     * @param string $action
     * @param Request $request
     * @param int $totalItems
     * @param PaginatorInterface $paginator
     * @param mixed $query
     * @return mixed
     */
    protected function getQueryResult(string $action, Request $request, int $totalItems, PaginatorInterface $paginator, $query) 
    {
        return $query->getQuery()->getResult();
    }
    
    /**
     * Count the number of entities
     * 
     * @param string $action
     * @param Request $request
     * @return int
     */
    protected function countEntities(string $action, Request $request): int
    {
        return $this->buildQueryEntities($action, $request)
            ->select('COUNT(e)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
    
    /**
     * BAse method for edit action
     * 
     * IMplemented by the method formEditAction, with action as 'edit'
     * 
     * @param Request $request
     * @param mixed $id
     * @return Response
     */
    public function edit(Request $request, $id): Response
    {
        return $this->formEditAction('edit', $request, $id);
    }
    
    /**
     * Base method for new action
     * 
     * Implemented by the method formNewAction, with action as 'new'
     * 
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        return $this->formCreateAction('new', $request);
    }
    
    /**
     * Base method for the view action.
     * 
     * Implemented by the method viewAction, with action as 'view'
     * 
     * @param Request $request
     * @param mixed $id
     * @return Response
     */
    public function view(Request $request, $id): Response
    {
        return $this->viewAction('view', $request, $id);
    }
    
    /**
     * The view action.
     * 
     * Some steps may be overriden during this process of rendering:
     * 
     * This method:
     * 
     * 1.  fetch the entity, using `getEntity`
     * 2.  launch `onPostFetchEntity`. If postfetch is an instance of Response,
     *     this response is returned.
     * 2.  throw an HttpNotFoundException if entity is null
     * 3.  check ACL using `checkACL` ;
     * 4.  launch `onPostCheckACL`. If the result is an instance of Response, 
     *     this response is returned ;
     * 5.  generate default template parameters:
     * 
     *     * `entity`: the fetched entity ;
     *     * `crud_name`: the crud name
     * 6.  Launch rendering, the parameter is fetch using `getTemplateFor` 
     *     The parameters may be personnalized using `generateTemplateParameter`.
     * 
     * @param string $action
     * @param Request $request
     * @param mixed $id
     * @return Response
     */
    protected function viewAction(string $action, Request $request, $id) 
    {
        $entity = $this->getEntity($action, $id, $request);
        
        $postFetch = $this->onPostFetchEntity($action, $request, $entity);
        
        if ($postFetch instanceof Response) {
            return $postFetch;
        }
        
        if (NULL === $entity) {
            throw $this->createNotFoundException(sprintf("The %s with id %s "
                . "is not found"), $this->getCrudName(), $id);
        }
        
        $response = $this->checkACL($action, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $response = $this->onPostCheckACL($action, $request, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $defaultTemplateParameters = [
            'entity' => $entity,
            'crud_name' => $this->getCrudName()
        ];
        
        return $this->render(
            $this->getTemplateFor($action, $entity, $request), 
            $this->generateTemplateParameter($action, $entity, $request, $defaultTemplateParameters)
            );
    }
    
    /**
     * The edit action.
     * 
     * Some steps may be overriden during this process of rendering:
     * 
     * This method:
     * 
     * 1.  fetch the entity, using `getEntity`
     * 2.  launch `onPostFetchEntity`. If postfetch is an instance of Response,
     *     this response is returned.
     * 2.  throw an HttpNotFoundException if entity is null
     * 3.  check ACL using `checkACL` ;
     * 4.  launch `onPostCheckACL`. If the result is an instance of Response, 
     *     this response is returned ;
     * 5.  generate a form using `createFormFor`, and handle request on this form;
     * 
     *     If the form is valid, the entity is stored and flushed, and a redirection
     *     is returned.
     * 
     *     In this case, those hooks are available:
     *     
     *     * onFormValid
     *     * onPreFlush
     *     * onPostFlush
     *     * onBeforeRedirectAfterSubmission. If this method return an instance of 
     *         Response, this response is returned.
     * 
     * 5.  generate default template parameters:
     * 
     *     * `entity`: the fetched entity ;
     *     * `crud_name`: the crud name ;
     *     * `form`: the formview instance.
     * 
     * 6.  Launch rendering, the parameter is fetch using `getTemplateFor` 
     *     The parameters may be personnalized using `generateTemplateParameter`.
     * 
     * @param string $action
     * @param Request $request
     * @param mixed $id
     * @param string $formClass
     * @param array $formOptions
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function formEditAction(string $action, Request $request, $id, string $formClass = null, array $formOptions = []): Response
    {
        $entity = $this->getEntity($action, $id, $request);
        
        if (NULL === $entity) {
            throw $this->createNotFoundException(sprintf("The %s with id %s "
                . "is not found"), $this->getCrudName(), $id);
        }
        
        $response = $this->checkACL($action, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $response = $this->onPostCheckACL($action, $request, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $form = $this->createFormFor($action, $entity, $formClass, $formOptions);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->onFormValid($entity, $form, $request);
            $em = $this->getDoctrine()->getManager();
            
            $this->onPreFlush($action, $entity, $form, $request);
            $em->flush();
            $this->onPostFlush($action, $entity, $form, $request);
            
            $this->addFlash('success', $this->generateFormSuccessMessage($action, $entity));
            
            $result = $this->onBeforeRedirectAfterSubmission($action, $entity, $form, $request);
            
            if ($result instanceof Response) {
                return $result;
            }
            
            return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_index');
            
        } elseif ($form->isSubmitted()) {
            $this->addFlash('error', $this->generateFormErrorMessage($action, $form));
        }
        
        $defaultTemplateParameters = [
            'form' => $form->createView(),
            'entity' => $entity,
            'crud_name' => $this->getCrudName()
        ];
        
        return $this->render(
            $this->getTemplateFor($action, $entity, $request), 
            $this->generateTemplateParameter($action, $entity, $request, $defaultTemplateParameters)
            );
    }
    
    /**
     * The new (or creation) action.
     * 
     * Some steps may be overriden during this process of rendering:
     * 
     * This method:
     * 
     * 1.  Create or duplicate an entity:
     * 
     *     If the `duplicate` parameter is present, the entity is duplicated 
     *     using the `duplicate` method.
     * 
     *     If not, the entity is created using the `create` method.
     * 3.  check ACL using `checkACL` ;
     * 4.  launch `onPostCheckACL`. If the result is an instance of Response, 
     *     this response is returned ;
     * 5.  generate a form using `createFormFor`, and handle request on this form;
     * 
     *     If the form is valid, the entity is stored and flushed, and a redirection
     *     is returned.
     * 
     *     In this case, those hooks are available:
     *     
     *     * onFormValid
     *     * onPreFlush
     *     * onPostFlush
     *     * onBeforeRedirectAfterSubmission. If this method return an instance of 
     *         Response, this response is returned.
     * 
     * 5.  generate default template parameters:
     * 
     *     * `entity`: the fetched entity ;
     *     * `crud_name`: the crud name ;
     *     * `form`: the formview instance.
     * 
     * 6.  Launch rendering, the parameter is fetch using `getTemplateFor` 
     *     The parameters may be personnalized using `generateTemplateParameter`.
     * 
     * @param string $action
     * @param Request $request
     * @param type $formClass
     * @return Response
     */
    protected function formCreateAction(string $action, Request $request, $formClass = null): Response
    {
        if ($request->query->has('duplicate')) {
            $entity = $this->duplicateEntity($action, $request);
        } else {
            $entity = $this->createEntity($action, $request);
        }
        
        $response = $this->checkACL($action, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $response = $this->onPostCheckACL($action, $request, $entity);
        if ($response instanceof Response) {
            return $response;
        }
        
        $form = $this->createFormFor($action, $entity, $formClass);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->onFormValid($entity, $form, $request);
            $em = $this->getDoctrine()->getManager();
            
            $this->onPrePersist($action, $entity, $form, $request);
            $em->persist($entity);
            $this->onPostPersist($action, $entity, $form, $request);
            
            $this->onPreFlush($action, $entity, $form, $request);
            $em->flush();
            $this->onPostFlush($action, $entity, $form, $request);
            $this->getPaginatorFactory();
            $this->addFlash('success', $this->generateFormSuccessMessage($action, $entity));
            
            $result = $this->onBeforeRedirectAfterSubmission($action, $entity, $form, $request);
            
            if ($result instanceof Response) {
                return $result;
            }
            
            return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_view', ['id' => $entity->getId()]);
            
        } elseif ($form->isSubmitted()) {
            $this->addFlash('error', $this->generateFormErrorMessage($action, $form));
        }
        
        $defaultTemplateParameters = [
            'form' => $form->createView(),
            'entity' => $entity,
            'crud_name' => $this->getCrudName()
        ];
        
        return $this->render(
            $this->getTemplateFor($action, $entity, $request), 
            $this->generateTemplateParameter($action, $entity, $request, $defaultTemplateParameters)
            );
    }
    
    /**
     * get the instance of the entity with the given id
     * 
     * @param string $id
     * @return object
     */
    protected function getEntity($action, $id, Request $request): ?object
    {
        return $this->getDoctrine()
            ->getRepository($this->getEntityClass())
            ->find($id);
    }
    
    /**
     * Duplicate an entity
     * 
     * @param string $action
     * @param Request $request
     * @return mixed
     */
    protected function duplicateEntity(string $action, Request $request)
    {
        $id = $request->query->get('duplicate_id', 0);
        $originalEntity = $this->getEntity($action, $id, $request);
        
        $this->getDoctrine()->getManager()
            ->detach($originalEntity);
        
        return $originalEntity;
    }
    
    /**
     * 
     * @return string the complete fqdn of the class
     */
    protected function getEntityClass(): string
    {
        return $this->crudConfig['class'];
    }
    
    /**
     * 
     * @return string the crud name
     */
    protected function getCrudName(): string
    {
        return $this->crudConfig['name'];
    }

    /**
     * check the acl. Called by every action.
     * 
     * By default, check the role given by `getRoleFor` for the value given in 
     * $entity.
     * 
     * Throw an \Symfony\Component\Security\Core\Exception\AccessDeniedHttpException
     * if not accessible.
     * 
     * @param string $action
     * @param mixed $entity
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedHttpException
     */
    protected function checkACL(string $action, $entity)
    {
        $this->denyAccessUnlessGranted($this->getRoleFor($action), $entity);
    }
    
    /**
     * get the role given from the config.
     * 
     * @param string $action
     * @return string
     */
    protected function getRoleFor($action)
    {
        if (NULL !== ($this->getActionConfig($action)['role'])) {
            return $this->getActionConfig($action)['role'];
        }
        
        return $this->buildDefaultRole($action);
    }

    /**
     * build a default role name, using the crud resolver.
     * 
     * This method should not be overriden. Override `getRoleFor` instead.
     * 
     * @param string $action
     * @return string
     */
    protected function buildDefaultRole($action)
    {
        return $this->getCrudResolver()->buildDefaultRole($this->getCrudName(), 
            $action);
    }
    
    /**
     * get the default form class from config
     * 
     * @param string $action
     * @return string the FQDN of the form class
     */
    protected function getFormClassFor($action)
    {
        if ($action === 'delete') {
            return $this->crudConfig[$action]['form_class']
            ?? $this->getDefaultDeleteFormClass($action);
        }
        
        return $this->crudConfig[$action]['form_class']
            ?? $this->crudConfig['form_class'];
    }
    
    protected function getDefaultDeleteFormClass($action)
    {
        return CRUDDeleteEntityForm::class;
    }
    
    /**
     * Create a form
     * 
     * use the method `getFormClassFor`
     * 
     * A hook is available: `customizeForm` allow you to customize the form
     * if needed.
     * 
     * It is preferable to override customizeForm instead of overriding
     * this method.
     * 
     * @param string $action
     * @param mixed $entity
     * @param string $formClass
     * @param array $formOptions
     * @return FormInterface
     */
    protected function createFormFor(string $action, $entity, string $formClass = null, array $formOptions = []): FormInterface
    {
        $formClass = $formClass ?? $this->getFormClassFor($action);
        
        $form = $this->createForm($formClass, $entity, $formOptions);
        
        $this->customizeForm($action, $form);
        
        return $form;
    }
    
    /**
     * Customize the form created by createFormFor.
     * 
     * @param string $action
     * @param FormInterface $form
     */
    protected function customizeForm(string $action, FormInterface $form)
    {

    }
    
    /**
     * Generate a message which explains an error about the form.
     * 
     * Used in form actions
     * 
     * @param string $action
     * @param FormInterface $form
     * @return string
     */
    protected function generateFormErrorMessage(string $action, FormInterface $form): string
    {
        $msg = 'This form contains errors';
        
        return $this->getTranslator()->trans($msg);
    }
    
    /**
     * Generate a success message when a form could be flushed successfully
     * 
     * @param string $action
     * @param mixed $entity
     * @return string
     */
    protected function generateFormSuccessMessage($action, $entity): string
    {
        switch ($action) {
            case 'edit':
                $msg = "crud.edit.success";
                break;
            case 'new':
                $msg = "crud.new.success";
                break;
            case 'delete':
                $msg = "crud.delete.success";
                break;
            default:
                $msg = "crud.default.success";
        }
        
        return $this->getTranslator()->trans($msg);
    }
    
    /**
     * Customize template parameters.
     * 
     * @param string $action
     * @param mixed $entity
     * @param Request $request
     * @param array $defaultTemplateParameters
     * @return array
     */
    protected function generateTemplateParameter(
        string $action, 
        $entity, 
        Request $request, 
        array $defaultTemplateParameters = []
    ) {
        return $defaultTemplateParameters;
    }

    /**
     * Create an entity.
     * 
     * @param string $action
     * @param Request $request
     * @return object
     */
    protected function createEntity(string $action, Request $request): object
    {
        $type = $this->getEntityClass();
        
        return new $type;
    }
    
    /**
     * Get the template for the current crud.
     * 
     * This template may be defined in configuration. If any template are 
     * defined, return the default template for the actions new, edit, index, 
     * and view.
     * 
     * @param string $action
     * @param mixed $entity the entity for the current request, or an array of entities
     * @param Request $request
     * @return string the path to the template
     * @throws \LogicException if no template are available
     */
    protected function getTemplateFor($action, $entity, Request $request)
    {
        if ($this->hasCustomTemplate($action, $entity, $request)) {
            return $this->getActionConfig($action)['template'];
        }
        
        switch ($action) {
            case 'new':
                return '@ChillMain/CRUD/new.html.twig';
            case 'edit': 
                return '@ChillMain/CRUD/edit.html.twig';
            case 'index':
                return '@ChillMain/CRUD/index.html.twig';
            case 'view':
                return '@ChillMain/CRUD/view.html.twig';
            case 'delete':
                return '@ChillMain/CRUD/delete.html.twig';
            default:
                throw new \LogicException("the view for action $action is not "
                    . "defined. You should override ".__METHOD__." to add this "
                    . "action");
        }
    }
    
    protected function hasCustomTemplate($action, $entity, Request $request): bool
    {
        return !empty($this->getActionConfig($action)['template']);
    }
    
    protected function onPreFlush(string $action, $entity, FormInterface $form, Request $request)
    {
    }

    protected function onPostFlush(string $action, $entity, FormInterface $form, Request $request)
    {
    }

    protected function onPrePersist(string $action, $entity, FormInterface $form, Request $request)
    {
    }
    
    protected function onPostPersist(string $action, $entity, FormInterface $form, Request $request)
    {
    }
    
    protected function onPostFetchEntity($action, Request $request, $entity): ?Response
    {
        return null;
    }
    
    protected function onPostCheckACL($action, Request $request, $entity): ?Response
    {
        return null;
    }
    
    protected function onFormValid(object $entity, FormInterface $form, Request $request)
    {
    }
    
    /**
     * Return a redirect response depending on the value of submit button.
     * 
     * The handled values are :
     * 
     * * save-and-close: return to index of current crud ;
     * * save-and-new: return to new page of current crud ;
     * * save-and-view: return to view page of current crud ;
     * 
     * @param string $action
     * @param mixed $entity
     * @param FormInterface $form
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function onBeforeRedirectAfterSubmission(string $action, $entity, FormInterface $form, Request $request)
    {
        $next = $request->request->get("submit", "save-and-close");

        switch ($next) {
            case "save-and-close":
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_index');
            case "save-and-new":
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_new', $request->query->all());
            default:
                return $this->redirectToRoute('chill_crud_'.$this->getCrudName().'_view', [
                    'id' => $entity->getId()
                    ]);
        }
    }
    
    protected function getActionConfig(string $action)
    {
        return $this->crudConfig['actions'][$action];
    }

    protected function getPaginatorFactory(): PaginatorFactory
    {
        return $this->get(PaginatorFactory::class);
    }
    
    protected function getTranslator(): TranslatorInterface
    {
        return $this->container->get('translator');
    }
    
    protected function getAuthorizationHelper(): AuthorizationHelper
    {
        return $this->container->get(AuthorizationHelper::class);
    }
    
    protected function getReachableCenters(Role $role, Scope $scope = null) 
    {
        return $this->getAuthorizationHelper()
            ->getReachableCenters($this->getUser(), $role, $scope)
            ;
    }
    
    protected function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->get(EventDispatcherInterface::class);
    }
    
    protected function getCrudResolver(): Resolver
    {
        return $this->get(Resolver::class);
    }
    
    public static function getSubscribedServices()
    {
        return \array_merge(
            parent::getSubscribedServices(),
            [
                PaginatorFactory::class => PaginatorFactory::class,
                'translator' => TranslatorInterface::class,
                AuthorizationHelper::class => AuthorizationHelper::class,
                EventDispatcherInterface::class => EventDispatcherInterface::class,
                Resolver::class => Resolver::class,
            ]
        );
    }
}
