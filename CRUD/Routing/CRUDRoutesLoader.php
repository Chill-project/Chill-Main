<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2019, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\CRUD\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


/**
 * Load the route for CRUD
 *
 */
class CRUDRoutesLoader
{
    protected $config = [];
    
    public function __construct($config)
    {
        $this->config = $config;
    }
    
    public function load()
    {
        $collection = new RouteCollection();
        
        foreach ($this->config as $config) {
            $collection->addCollection($this->loadConfig($config));
        }
        
        return $collection;
    }
    
    protected function loadConfig($config): RouteCollection
    {
        $collection = new RouteCollection();
        foreach ($config['actions'] as $name => $action) {
            $defaults = [
                '_controller' => 'cscrud_'.$config['name'].'_controller'.':'.($action['controller_action'] ?? $name)
            ];
            
            if ($name === 'index') {
                $path = "{_locale}".$config['base_path'];
                $route = new Route($path, $defaults);
            } elseif ($name === 'new') {
                $path = "{_locale}".$config['base_path'].'/'.$name;
                $route = new Route($path, $defaults);
            } else {
                $path = "{_locale}".$config['base_path'].($action['path'] ?? '/{id}/'.$name);
                $requirements = $action['requirements'] ?? [
                    '{id}' => '\d+'
                ];
                $route = new Route($path, $defaults, $requirements);
            }
            
            $collection->add('chill_crud_'.$config['name'].'_'.$name, $route);
        }
        
        return $collection;
    }
}
