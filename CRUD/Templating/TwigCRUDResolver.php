<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2019, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\CRUD\Templating;

use Chill\MainBundle\CRUD\Resolver\Resolver;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Twig\Environment;

/**
 * Twig filters to display data in crud template
 *
 */
class TwigCRUDResolver extends AbstractExtension
{
    /**
     *
     * @var Resolver
     */
    protected $resolver;
    
    function __construct(Resolver $resolver)
    {
        $this->resolver = $resolver;
    }
    
    public function getFunctions()
    {
        return [
            new TwigFunction('chill_crud_config', [$this, 'getConfig'],
                ['is_safe' => 'html']),
            new TwigFunction('chill_crud_action_exists', [$this, 'hasAction'],
                []),
        ];
    }
    
    public function getConfig($configKey, $crudName, $action = null)
    {
        return $this->resolver->getConfigValue($configKey, $crudName, $action);
    }
    
    public function hasAction($crudName, $action)
    {
        return $this->resolver->hasAction($crudName, $action);
    }

}
