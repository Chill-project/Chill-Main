<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Repository;

use Chill\MainBundle\Entity\GroupCenter;
use Chill\MainBundle\Entity\User;

/**
 * 
 *
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function countByUsernameOrEmail($pattern)
    {
        $qb = $this->queryByUsernameOrEmail($pattern);
        
        $qb->select('COUNT(u)');
        
        return (int) $qb->getQuery()->getSingleScalarResult();
    }
    
    public function findByUsernameOrEmail($pattern) 
    {
        $qb = $this->queryByUsernameOrEmail($pattern);
        
        return $qb->getQuery()->getResult();
    }
    
    public function findOneByUsernameOrEmail($pattern)
    {
        $qb = $this->queryByUsernameOrEmail($pattern);
        
        return $qb->getQuery()->getSingleResult();
    }
    
    /**
     * Get the users having a specific flags
     * 
     * If provided, only the users amongst "filtered users" are searched. This 
     * allows to make a first search amongst users based on role and center 
     * and, then filter those users having some flags.
     * 
     * @param \Chill\MainBundle\Entity\User[] $amongstUsers
     */
    public function findUsersHavingFlags($flag, $amongstUsers = [])
    {
        $gcs = $this->_em->createQuery("SELECT DISTINCT gc "
            . "FROM ".GroupCenter::class." gc "
            . "JOIN gc.permissionsGroup pg "
            . "WHERE "
            . "JSONB_EXISTS_IN_ARRAY(pg.flags, :flag) = :true ")
            ->setParameters([
                'true' => true,
                'flag' => $flag
            ])
            ->getResult();
        
        if (count($gcs) === 0) {
            return [];
        }
        
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('DISTINCT u')
            ->from(User::class, 'u')
            ->where("u.enabled = 'TRUE'")
            ;
        
        $orx = $qb->expr()->orX();
        foreach($gcs as $i => $gc) {
            $orx->add(':gc_'.$i.' MEMBER OF u.groupCenters');
            $qb->setParameter('gc_'.$i, $gc);
        }
        
        $qb->andWhere($orx);
        
        if (count($amongstUsers) > 0) {
            $qb
                ->andWhere($qb->expr()->in('u', ':amongstUsers'))
                ->setParameter('amongstUsers', $amongstUsers)
                ;
        }
        
        return $qb->getQuery()->getResult();
    }
    
    protected function queryByUsernameOrEmail($pattern)
    {
        $qb = $this->createQueryBuilder('u');
        
        $searchByPattern = $qb->expr()->orX();
        
        $searchByPattern
            ->add($qb->expr()->eq('u.usernameCanonical', 'LOWER(UNACCENT(:pattern))'))
            ->add($qb->expr()->eq('u.emailCanonical', 'LOWER(UNACCENT(:pattern))'))
            ;
        
        $qb
            ->where($searchByPattern)
            ->setParameter('pattern', $pattern)
            ;
        
        return $qb;
    }
}
