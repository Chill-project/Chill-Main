<?php

namespace Chill\MainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Chill\MainBundle\DependencyInjection\SearchableServicesCompilerPass;
use Chill\MainBundle\DependencyInjection\ConfigConsistencyCompilerPass;
use Chill\MainBundle\DependencyInjection\TimelineCompilerClass;
use Chill\MainBundle\DependencyInjection\RoleProvidersCompilerPass;
use Chill\MainBundle\DependencyInjection\CompilerPass\ExportsCompilerPass;
use Chill\MainBundle\DependencyInjection\CompilerPass\WidgetsCompilerPass;
use Chill\MainBundle\DependencyInjection\CompilerPass\NotificationCounterCompilerPass;
use Chill\MainBundle\DependencyInjection\CompilerPass\MenuCompilerPass;
use Chill\MainBundle\DependencyInjection\CompilerPass\ACLFlagsCompilerPass;
use Chill\MainBundle\DependencyInjection\CompilerPass\GroupingCenterCompilerPass;
use Chill\MainBundle\Templating\Entity\CompilerPass as RenderEntityCompilerPass;


class ChillMainBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new SearchableServicesCompilerPass());
        $container->addCompilerPass(new ConfigConsistencyCompilerPass());
        $container->addCompilerPass(new TimelineCompilerClass());
        $container->addCompilerPass(new RoleProvidersCompilerPass());
        $container->addCompilerPass(new ExportsCompilerPass());
        $container->addCompilerPass(new WidgetsCompilerPass());
        $container->addCompilerPass(new NotificationCounterCompilerPass());
        $container->addCompilerPass(new MenuCompilerPass());
        $container->addCompilerPass(new ACLFlagsCompilerPass());
        $container->addCompilerPass(new GroupingCenterCompilerPass());
        $container->addCompilerPass(new RenderEntityCompilerPass());
    }
}
