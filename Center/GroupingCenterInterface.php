<?php
/*
 * 
 */
namespace Chill\MainBundle\Center;

/**
 * Interface to declare a groups of centers.
 * 
 * This interface is used to declare a groups of centers in 
 * `Chill\MainBundle\Form\Export\PickCenterType`.
 * 
 */
interface GroupingCenterInterface
{
    /**
     * @return string[]
     */
    public function getGroups($authorizedCenters = null): array;
    
    /**
     * 
     * @param string $group
     * @return \Chill\MainBundle\Entity\Center[]
     */
    public function getCentersForGroup($group);
}
