<?php
/*
 * 
 */
namespace Chill\MainBundle\Export;

/**
 * Add a grouping option to exports. 
 * 
 * **usage**: the exports will be grouped under the key given by the `getGroup` 
 * method. 
 */
interface GroupedExportInterface
{
    public function getGroup(): string;
}
