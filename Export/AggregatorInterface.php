<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Export;

/**
 * Interface for Aggregators. 
 * 
 * Aggregators gather result of a query. Most of the time, it will add
 * a GROUP BY clause.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface AggregatorInterface extends ModifierInterface
{ 
    /**
     * give the list of keys the current export added to the queryBuilder in 
     * self::initiateQuery
     * 
     * Example: if your query builder will contains `SELECT count(id) AS count_id ...`,
     * this function will return `array('count_id')`.
     * 
     * @param mixed[] $data the data from the export's form (added by self::buildForm)
     */
    public function getQueryKeys($data);
    
    /**
     * get a callable which will be able to transform the results into
     * viewable and understable string.
     * 
     * The callable will have only one argument: the `value` to translate. 
     * 
     * The callable should also be able to return a key `_header`, which
     * will contains the header of the column. 
     * 
     * The string returned **must** be already translated if necessary, 
     * **with an exception** for the string returned for `_header`.
     * 
     * Example : 
     * 
     * ```
     * protected $translator;
     * 
     * public function getLabels($key, array $values, $data)
     * {
     *      return function($value) {
     *          case $value
     *          {
     *              case '_header' : 
     *                  return 'my header not translated'; 
     *              case true:
     *                  return $this->translator->trans('true');
     *              case false:
     *                  return $this->translator->trans('false');
     *              default:
     *                  // this should not happens !
     *                  throw new \LogicException();
     *          }
     * }
     * ```
     * 
     * **Note:** Why each string must be translated with an exception for 
     * the `_header` ? For performance reasons: most of the value will be number
     * which do not need to be translated, or value already translated in
     * database. But the header must be, in every case, translated.
     * 
     * @param string $key The column key, as added in the query
     * @param mixed[] $values The values from the result. if there are duplicates, those might be given twice. Example: array('FR', 'BE', 'CZ', 'FR', 'BE', 'FR')
     * @param mixed $data The data from the export's form (as defined in `buildForm`
     * @return \Closure where the first argument is the value, and the function should return the label to show in the formatted file. Example : `function($countryCode) use ($countries) { return $countries[$countryCode]->getName(); }`
     */
    public function getLabels($key, array $values, $data);

}
