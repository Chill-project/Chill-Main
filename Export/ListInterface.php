<?php

namespace Chill\MainBundle\Export;

/**
 * Define methods to export list.
 * 
 * This interface is a specification of export interface 
 * and should be used when the export does not supports aggregators
 * (and list does not support aggregation on their data).
 * 
 * When used, the `ExportManager` will not handle aggregator for this class.
 */
interface ListInterface extends ExportInterface
{

}
