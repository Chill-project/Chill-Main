<?php

/*
 * Copyright (C) 2015 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Export;


/**
 * Interface for filters. 
 * 
 * Filter will filter result on the query initiated by Export. Most of the time, 
 * it will add a `WHERE` clause on this query.
 * 
 * Filters should not add column in `SELECT` clause.
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface FilterInterface extends ModifierInterface
{
    
    const STRING_FORMAT = 'string';
    
    /**
     * Describe the filtering action.
     * 
     * The description will be inserted in report to remains to the user the 
     * filters applyied on this report.
     * 
     * Should return a statement about the filtering action, like : 
     * "filtering by date: from xxxx-xx-xx to xxxx-xx-xx" or 
     * "filtering by nationality: only Germany, France"
     * 
     * The format will be determined by the $format. Currently, only 'string' is
     * supported, later some 'html' will be added. The filter should always 
     * implement the 'string' format and fallback to it if other format are used.
     * 
     * If no i18n is necessery, or if the filter translate the string by himself,
     * this function should return a string. If the filter does not do any translation,
     * the return parameter should be an array, where 
     * 
     * - the first element is the string, 
     * - and the second an array of parameters (may be an empty array)
     * - the 3rd the domain (optional)
     * - the 4th the locale (optional)
     * 
     * Example: `array('my string with %parameter%', ['%parameter%' => 'good news'], 'mydomain', 'mylocale')`
     * 
     * @param array $data
     * @param string $format the format
     * @return string|array a string with the data or, if translatable, an array where first element is string, second elements is an array of arguments
     */
    public function describeAction($data, $format = 'string');

}
