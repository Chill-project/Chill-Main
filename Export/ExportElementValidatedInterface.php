<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Export;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Add a validation method to validate data collected by Export Element
 * 
 * Implements this interface on other `ExportElementInterface` to validate
 * the form submitted by users.
 * 
 * **note**: When this interface is implemented on filters or aggregators,
 * the form is validated **only** if the filter/aggregator is `enabled` by the
 * user.
 * 
 * @link http://symfony.com/doc/current/validation/custom_constraint.html#class-constraint-validator Example of building violations
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface ExportElementValidatedInterface
{
    /**
     * validate the form's data and, if required, build a contraint
     * violation on the data.
     * 
     * @param mixed $data the data, as returned by the user
     * @param ExecutionContextInterface $context
     */
    public function validateForm($data, ExecutionContextInterface $context);
}
