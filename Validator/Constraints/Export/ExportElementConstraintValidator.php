<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Validator\Constraints\Export;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Chill\MainBundle\Export\ExportElementValidatedInterface;

/**
 * This validator validate the _export element_ if this element implements
 * {@link ExportElementValidatedInterface}. 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ExportElementConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {        
        if ($constraint->element instanceof ExportElementValidatedInterface) {
            if ($value["enabled"] === true) {
                $constraint->element->validateForm($value["form"], $this->context);
            } 
        }
    }
}
