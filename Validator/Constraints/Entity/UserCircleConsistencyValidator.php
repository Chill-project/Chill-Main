<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Validator\Constraints\Entity;

use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Chill\MainBundle\Entity\HasScopeInterface;

/**
 * 
 *
 */
class UserCircleConsistencyValidator extends ConstraintValidator
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $autorizationHelper;
    
    function __construct(AuthorizationHelper $autorizationHelper)
    {
        $this->autorizationHelper = $autorizationHelper;
    }

    
    /**
     * 
     * @param object $value
     * @param UserCircleConsistency $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $user \Chill\MainBundle\Entity\User */
        $user = \call_user_func([$value, $constraint->getUserFunction ]);
        
        if ($user === null) {
            return;
        }
        
        if (FALSE === $this->autorizationHelper->userHasAccess($user, $value, $constraint->role)) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ username }}', $user->getUsername())
                ->atPath($constraint->path)
                ->addViolation()
                ;
        }
    }
}
