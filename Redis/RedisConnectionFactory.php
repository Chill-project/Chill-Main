<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Redis;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class RedisConnectionFactory implements EventSubscriberInterface
{
    protected $host;
    
    protected $port;
    
    protected $timeout;
    
    /**
     *
     * @var Redis
     */
    protected $redis;
    
    public function __construct($parameters)
    {
        $this->host = $parameters['host'];
        $this->port = $parameters['port'];
        $this->timeout = $parameters['timeout'];
    }
    
    
    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.finish_request' => [
                [ 'onKernelFinishRequest' ]
            ]
        ];
    }
    
    public function create()
    {
        $redis = $this->redis = new ChillRedis();
        
        $result = $redis->connect($this->host, $this->port, $this->timeout);
        
        if (FALSE === $result) {
            throw new \RuntimeException("Could not connect to redis instance");
        }
        
        return $redis;
    }
    
    public function onKernelFinishRequest()
    {
        if ($this->redis !== null) {
            $this->redis->close();
        }
    }

}
