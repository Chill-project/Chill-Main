<?php


/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Chill\MainBundle\Search\UnknowSearchDomainException;
use Chill\MainBundle\Search\UnknowSearchNameException;
use Chill\MainBundle\Search\ParsingException;
use Chill\MainBundle\Search\SearchInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *
 *
 * @author julien.fastre@champs-libres.coop
 * @author marc@champs-libres.coop
 */
class SearchController extends Controller
{
    
    public function searchAction(Request $request, $_format)
    {
        $pattern = $request->query->get('q', '');
        
        if ($pattern === ''){
            switch($_format) {
                case 'html': 
                    return $this->render('ChillMainBundle:Search:error.html.twig',
                          array(
                             'message' => $this->get('translator')->trans("Your search is empty. "
                                   . "Please provide search terms."),
                             'pattern' => $pattern
                          ));
                case 'json':
                    return new JsonResponse([
                        'results' => [],
                        'pagination' => [ 'more' => false ]
                    ]);
            }
        }
        
        $name = $request->query->get('name', NULL);
        
        try {
            if ($name === NULL) {
                if ($_format === 'json') {
                    return new JsonResponse('Currently, we still do not aggregate results '
                        . 'from different providers', JsonResponse::HTTP_BAD_REQUEST);
                }
                
                // no specific search selected. Rendering result in "preview" mode
                $results = $this->get('chill.main.search_provider')
                    ->getSearchResults(
                        $pattern,
                        0,
                        5,
                        array(SearchInterface::SEARCH_PREVIEW_OPTION => true)
                        );
            } else {
                // we want search on a specific search provider. Display full results.
                
                // create a paginator to get the startPage and stopPage
                /* @var $paginatorFactory \Chill\MainBundle\Pagination\PaginatorFactory */
                $paginatorFactory = $this->get('chill_main.paginator_factory');
                
                $results = [$this->get('chill.main.search_provider')
                    ->getResultByName(
                            $pattern,
                            $name,
                            $paginatorFactory->getCurrentPageFirstItemNumber(),
                            $paginatorFactory->getCurrentItemsPerPage(),
                            array(
                                SearchInterface::SEARCH_PREVIEW_OPTION => false,
                                SearchInterface::REQUEST_QUERY_PARAMETERS => $request
                                    ->get(SearchInterface::REQUEST_QUERY_KEY_ADD_PARAMETERS, [])
                                ),
                            $_format
                            )];
                
                if ($_format === 'json') {
                    return new JsonResponse(\reset($results));
                }
            }
        } catch (UnknowSearchDomainException $ex) {
            return $this->render('ChillMainBundle:Search:error.html.twig',
                  array(
                     "message" => $this->get('translator')->trans("The domain %domain% "
                           . "is unknow. Please check your search.", array('%domain%' => $ex->getDomain())),
                     'pattern' => $pattern
                  ));
        } catch (UnknowSearchNameException $ex) {
            throw $this->createNotFoundException("The name ".$ex->getName()." is not found");
        } catch (ParsingException $ex) {
            return $this->render('ChillMainBundle:Search:error.html.twig',
                  array(
                     "message" => $this->get('translator')->trans('Invalid terms').
                     ": ".$this->get('translator')->trans($ex->getMessage()),
                     'pattern' => $pattern
                  ));
        }
        
    
        return $this->render('ChillMainBundle:Search:list.html.twig',
              array('results' => $results, 'pattern' => $pattern)
              );
    }
    
    public function advancedSearchListAction(Request $request)
    {
        /* @var $variable Chill\MainBundle\Search\SearchProvider */
        $searchProvider = $this->get('chill.main.search_provider');
        $advancedSearchProviders = $searchProvider
            ->getHasAdvancedFormSearchServices();
        
        if(\count($advancedSearchProviders) === 1) {
            \reset($advancedSearchProviders);
            
            return $this->redirectToRoute('chill_main_advanced_search', [
                'name' => \key($advancedSearchProviders)
            ]);
        }
        
        return $this->render('ChillMainBundle:Search:choose_list.html.twig');
    }
    
    public function advancedSearchAction($name, Request $request)
    {
        try {
            /* @var $variable Chill\MainBundle\Search\SearchProvider */
            $searchProvider = $this->get('chill.main.search_provider');
            /* @var $variable Chill\MainBundle\Search\HasAdvancedSearchFormInterface */
            $search = $this->get('chill.main.search_provider')
                        ->getHasAdvancedFormByName($name);
        
        } catch (\Chill\MainBundle\Search\UnknowSearchNameException $e) {
            throw $this->createNotFoundException("no advanced search for "
                . "$name");
        }
        
        if ($request->query->has('q')) {
            $data = $search->convertTermsToFormData($searchProvider->parse(
                $request->query->get('q')));
        }
        
        $form = $this->createAdvancedSearchForm($name, $data ?? []);
        
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                $pattern = $this->get('chill.main.search_provider')
                    ->getHasAdvancedFormByName($name)
                    ->convertFormDataToQuery($form->getData());

                return $this->redirectToRoute('chill_main_search', [
                    'q' => $pattern, 'name' => $name
                ]);
            }
        } 
        
        return $this->render('ChillMainBundle:Search:advanced_search.html.twig',
            [
                'form' => $form->createView(),
                'name' => $name,
                'title' => $search->getAdvancedSearchTitle()
            ]);
    }
    
    protected function createAdvancedSearchForm($name, array $data = [])
    {
        $builder = $this
            ->get('form.factory')
            ->createNamedBuilder(
                null, 
                FormType::class, 
                $data, 
                [ 'method' => Request::METHOD_POST ]
            );

        $this->get('chill.main.search_provider')
            ->getHasAdvancedFormByName($name)
            ->buildForm($builder)
            ;
        
        $builder->add('submit', SubmitType::class, [
            'label' => 'Search'
        ]);
        
        return $builder->getForm();
    }
    
}
