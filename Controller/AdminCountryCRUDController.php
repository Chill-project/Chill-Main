<?php

namespace Chill\MainBundle\Controller;

use Chill\MainBundle\CRUD\Controller\CRUDController;
use Chill\MainBundle\Entity\Country;
use Chill\MainBundle\Pagination\PaginatorFactory;

/**
 * 
 *
 */
class AdminCountryCRUDController extends CRUDController
{
    
    function __construct(PaginatorFactory $paginator)
    {
        $this->paginatorFactory = $paginator;
    }
}
