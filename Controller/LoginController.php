<?php

namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends Controller
{
    /**
     *
     * @var AuthenticationUtils
     */
    protected $helper;
    
    public function __construct(AuthenticationUtils $helper)
    {
        $this->helper = $helper;
    }
    
    /**
     * Show a login form
     * 
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request)
    {

        return $this->render('ChillMainBundle:Login:login.html.twig', array(
            'last_username' => $this->helper->getLastUsername(),
            'error'         => $this->helper->getLastAuthenticationError()
        ));  
    }
    
    public function LoginCheckAction(Request $request)
    {
        
    }

}
