<?php
/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2018, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Chill\MainBundle\Entity\PostalCode;
use Symfony\Component\HttpFoundation\JsonResponse;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Doctrine\ORM\Query;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PostalCodeController extends Controller
{
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }

    
    /**
     * 
     * @Route(
     *  "{_locale}/postalcode/search"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function searchAction(Request $request)
    {
        $pattern = $request->query->getAlnum('q', '');
        
        if (empty($pattern)) {
            return new JsonResponse(["results" => [], "pagination" => [ "more" => false]]);
        }
        
        $query = $this->getDoctrine()->getManager()
            ->createQuery(sprintf(
                "SELECT p.id AS id, p.name AS name, p.code AS code, "
                . "country.name AS country_name, "
                . "country.countryCode AS country_code "
                . "FROM %s p "
                . "JOIN p.country country "
                . "WHERE LOWER(p.name) LIKE LOWER(:pattern) OR LOWER(p.code) LIKE LOWER(:pattern) "
                . "ORDER BY code"
                , PostalCode::class)
                )
            ->setParameter('pattern', '%'.$pattern.'%')
            ->setMaxResults(30)
            ;
        
        $results = \array_map(function($row) {
            $row['country_name'] = $this->translatableStringHelper->localize($row['country_name']);
            $row['text'] = $row['code']." ".$row["name"]." (".$row['country_name'].")";
            
            return $row;
        }, $query->getResult(Query::HYDRATE_ARRAY));
        
        return new JsonResponse([ 'results' => $results, "pagination" => [ "more" => false ] ]);
    }
}
